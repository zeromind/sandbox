<?php /** @var string $CONFIRM_URL */ ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <style type="text/css">
        html, body {margin: 0 !important; font-family: Tahoma, sans-serif; color: #2C2C2C;}
    </style>
</head>
<body style="margin:0; padding:0; background: #F2F2F2;">
<center>
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td height="10" style="font-size:25px; line-height:25px;" class="mobileOn">&nbsp;</td>
        </tr>
        <tr>
            <td align="center" valign="top">

                <table width="600" cellspacing="0" cellpadding="0" border="0" align="center" style="max-width:600px; width:100%;" bgcolor="#FFFFFF">
                    <tr>
                        <td valign="middle" align="center" height="240" style="border-bottom: 15px solid #F2F2F2;">
                            <img src="https://i.imgur.com/H855fLw.jpg" alt="HEADER" height="240" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top" style="padding:10px;">

                            <table width="600" cellspacing="0" cellpadding="0" border="0" align="center" style="max-width:600px; width:100%;">
                                <tr>
                                    <td align="center" valign="top" style="padding:10px; font-size: 14px;">
                                        Недавно вы зарегистрировались на нашем сайте.<br />
                                        Что бы продолжить регистрацию, вам необходимо подтвердить эту почту.<br />
                                        <br />
                                        <font color="#a5a5a5">Нажмите на кнопку ниже.</font>
                                    </td>
                                </tr>
                            </table>

                        </td>
                    </tr>
                </table>

            </td>
        </tr>
        <tr>
            <td height="25" style="font-size:25px; line-height:25px;" class="mobileOn">&nbsp;</td>
        </tr>
        <tr>
            <td align="center" valign="middle">

                <table width="200" cellpadding="0" cellspacing="0" border="0" bgcolor="#53A3E1" style="border-radius:4px;">
                    <tr>
                        <td align="center" valign="middle" style="font-size:12px; font-weight:bold; text-transform: uppercase;">

                            <a href="<?= $CONFIRM_URL; ?>" target="_blank" style="color:#ffffff; display: inline-block; text-decoration: none; line-height:44px; width:200px;">
                                <?= Yii::t('app', "Подтвердить"); ?>
                            </a>

                        </td>
                    </tr>
                </table>

            </td>
        </tr>
        <tr>
            <td height="25" style="font-size:25px; line-height:25px;" class="mobileOn">&nbsp;</td>
        </tr>
    </table>
</center>
</body>
</html>