<?php namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\Url;

use app\components\MultilingualBehavior;
use app\components\MultilingualQuery;

/**
 * Class Menu
 * @package app\models
 *
 * @property integer $id
 * @property integer $parent_id
 * @property integer $sort_order
 * @property string $name
 * @property string $url
 * @property integer $depth
 * @property integer $group
 *
 * @property array $parents
 * @property Menu $child
 * @property array $menuGroups
 * @property array $menuItems
 */
class Menu extends ActiveRecord
{
    const   GROUP_HEADER = 1,
            GROUP_FOOTER = 2;

    public static function tableName()
    {
        return 'menu';
    }

    public function rules()
    {
        return [
            [['name', 'url', 'sort_order', 'parent_id'], 'required'],
            [['parent_id', 'sort_order', 'depth', 'group'], 'integer'],
            [['name', 'url'], 'string', 'max' => 255],
            [['parent_id', 'depth', 'group', 'sort_order'], 'default', 'value' => 0]
        ];
    }

    public function attributeLabels()
    {
        return [
            'name'          => Yii::t('app', 'Текст'),
            'url'           => Yii::t('app', 'Ссылка'),
            'parent_id'     => Yii::t('app', 'Родитель'),
            'sort_order'    => Yii::t('app', 'Порядок'),
            'depth'         => Yii::t('app', 'Глубина'),
            'group'         => Yii::t('app', 'Группа')
        ];
    }

    public function behaviors()
    {
        return [
            'ml' => [
                'class' => MultilingualBehavior::class,
                'languages' => Languages::getBehaviorsLanguages(),
                'defaultLanguage' => Languages::getCurrentLanguage()->locale,
                'langForeignKey' => 'original_id',
                'tableName' => "{{%menu_lang}}",
                'attributes' => ['name']
            ]
        ];
    }

    public function getParents($all = false)
    {
        if ($all || $this->isNewRecord) {
            $items = Menu::find()->multilingual()->all();
        } else {
            $items = Menu::find()->where(['!=', 'id', $this->id])->multilingual()->indexBy('id')->all();
        }

        return $items;
    }

    public static function find()
    {
        $q = new MultilingualQuery(get_called_class());
        $q->localized();

        return $q;
    }

    public static function getChild($parent_id)
    {
        return Menu::find()->where(['parent_id' => $parent_id])->multilingual()->indexBy('id')->all();
    }

    public static function getMenuGroups()
    {
        return [
            self::GROUP_HEADER => Yii::t('app', 'Главное меню'),
            self::GROUP_FOOTER => Yii::t('app', 'Меню в подвале')
        ];
    }

    public static function getMenuItems()
    {
        if (Yii::$app->controller->system_cache == false) {
            $menu = false;
        } else {
            $menu = Yii::$app->cache->get('menu_' . Yii::$app->language);
        }

        if ($menu == false) {
            $menu = Menu::find()->orderBy('depth ASC, sort_order ASC')->multilingual()->indexBy('id')->all();
            $menu = self::Builder($menu);
            Yii::$app->cache->set('menu_' . Yii::$app->language, $menu);
        }

        return $menu;
    }

    /**
     * @param array $items
     *
     * @return array
     */
    private static function BuilderItems($items) : array
    {
        $_items = [];

        /** @var Menu $item */
        foreach ($items as $item) {
            if (substr($item->url, 0, 1) == '@') {
                $url = Url::toRoute(Yii::getAlias($item->url));
            } else {
                $url = Url::toRoute($item->url);
            }

            $active = $url == Yii::$app->request->url ? 'active ' : '';
            $child = self::getChild($item->id);

            if ($child && count($child) > 0 && $item->parent_id) {
                $template = '<a class="' . $active . 'dropdown-item dropdown-toggle" href="{url}">{label}</a>';
            } elseif ($item->parent_id && empty ($child)) {
                $template = '<a class="' . $active . 'dropdown-item" href="{url}">{label}</a>';
            } else {
                $template = '<a class="' . $active . 'menu-item-link" href="{url}">{label}</a>';
            }

            $_items[$item->group][$item->id] = [
                'label'     => $item->name,
                'url'       => $url,
                'options'   => [],
                'template'  => $template,
                'visible'   => true,
                'items'     => []
            ];

            if ($child && count($child) > 0) {
                if ($item->parent_id > 0) {
                    $_items[$item->group][$item->id]['options']['class'] .= ' dropdown-submenu';
                } else {
                    $_items[$item->group][$item->id]['options']['class'] .= ' dropdown';
                }

                $_items[$item->group][$item->id]['items'] = self::BuilderItems($child)[$item->group];
            }
        }

        return $_items;
    }

    /**
     * @param array $menu
     *
     * @return array
     */
    private static function Builder($menu) : array
    {
        $items = [];

        /** @var Menu $item */
        foreach ($menu as $item) if (empty ($item->parent_id)) $items[] = $item;

        $items = self::BuilderItems($items);

        return $items;
    }

    public static function flushCache()
    {
        $behaviors = Languages::getBehaviorsList();
        foreach ($behaviors as $title => $language) {
            Yii::$app->cache->delete('menu_' . $language);
        }
    }

    public function beforeDelete()
    {
        $child = self::getChild($this->id);
        if ($child && count($child) > 0) foreach ($child as $model) {
            $model->delete();
        }

        return parent::beforeDelete();
    }
}