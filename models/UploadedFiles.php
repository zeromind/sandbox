<?php namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\web\UploadedFile;

/**
 * This is the model class for table "uploaded_files".
 *
 * @property integer $id
 * @property string $file
 * @property integer $created_at
 * @property float $updated_at
 *
 * @property \yii\db\ActiveRecord $model
 */
class UploadedFiles extends \yii\db\ActiveRecord
{
    public $model = false;
    public $files = [];

    public $extensions = [
        'jpg', 'png', 'svg'
    ];

    public static function tableName()
    {
        return 'uploaded_files';
    }

    public function rules()
    {
        return [
            [['file'], 'required'],
            [['file'], 'string', 'max' => 512],
        ];
    }

    public function attributeLabels()
    {
        return [
            'file'  => Yii::t('app', 'Файл')
        ];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::class
        ];
    }

    public function uploadFiles()
    {
        if (!$this->model->validate()) return false;

        if (is_array($this->files) && count ($this->files) > 0)
        {
            /**
             * @var string $attribute
             * @var UploadedFile|array $file
             */
            foreach ($this->files as $attribute => $file)
            {
                if (gettype($file) == 'object' && get_class($file) == 'yii\web\UploadedFile')
                {
                    $file = $this->createFile($file, $attribute);

                    /** @var UploadedFiles|boolean $file */
                    if ($file) $this->model->$attribute = $file->id;
                }
                elseif (gettype($file) == 'array')
                {
                    $files = [];

                    /**
                     * @var integer $id
                     * @var UploadedFile $subFile
                     */
                    foreach ($file as $id => $subFile)
                    {
                        $subFile = $this->createFile($subFile, $attribute);

                        /** @var UploadedFiles|boolean $subFile */
                        if ($subFile) $files[] = $subFile->id;
                    }

                    $this->model->$attribute = implode(',', $files);
                }
            }
        }

        return $this->model;
    }

    /**
     * @param $file
     * @param $attribute
     * @return UploadedFiles|bool
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    private function createFile($file, $attribute)
    {
        if (!in_array($file->extension, $this->extensions))
        {
            $this->model->$attribute = $this->model->getOldAttribute($attribute);
            $this->model->addError($attribute, Yii::t('app', "Расширение файла не верное. ") . implode(', ', $this->extensions));
            return false;
        }

        $_path = Yii::getAlias('@webroot') . $this->model->getOldAttribute($attribute);
        $path = '/files/uploads/' . time() . '_' . strtoupper(substr(md5($file->baseName), 0, 10)) . '.' . $file->extension;
        $$attribute = $path;
        $path = Yii::getAlias('@webroot') . $path;

        if ($this->validatePath(Yii::getAlias('@webroot') . '/files/uploads/') && $file->saveAs($path))
        {
            $files = explode(',', $this->model->getOldAttribute($attribute));
            foreach ($files as $file_id) if (intval($file_id) > 0)
            {
                $tempFile = UploadedFiles::findOne($file_id);
                if ($tempFile && file_exists(Yii::getAlias('@webroot') . $tempFile->file) && $_path != Yii::getAlias('@webroot'))
                {
                    unlink(Yii::getAlias('@webroot') . $tempFile->file);
                    $tempFile->delete();
                }
            }

            $uploadedFile = new UploadedFiles();
            $uploadedFile->file = $$attribute;
            if ($uploadedFile->save()) return $uploadedFile;
        }
        else
        {
            $this->model->addError($attribute, Yii::t('app', "Не удалось загрузить файл"));
            return false;
        }

        $this->model->addError($attribute, Yii::t('app', "Не удалось сохранить файл"));
        return false;
    }

    public static function getCachedFile(int $file_id)
    {
        if (Yii::$app->controller->system_cache)
        {
            Yii::debug("Getting cache id: uploaded-file-{$file_id}", self::class);

            $file = Yii::$app->cache->get('uploaded-file-' . $file_id);
            if ($file && file_exists(Yii::getAlias('@webroot') . $file)) return $file;
            else
            {
                $uploadedFile = self::findOne($file_id);
                if ($uploadedFile && file_exists(Yii::getAlias('@webroot') . $uploadedFile->file)) {
                    Yii::debug("Updating cache id: uploaded-file-{$file_id}", self::class);
                    Yii::$app->cache->set('uploaded-file-' . $file_id, $uploadedFile->file);
                    return $uploadedFile->file;
                } elseif ($uploadedFile && !file_exists(Yii::getAlias('@webroot') . $uploadedFile->file)) {
                    $uploadedFile->delete();
                }
            }

            Yii::debug("Cache cannot be found by id: uploaded-file-{$file_id}", self::class);
        }

        $uploadedFile = self::findOne($file_id);
        if ($uploadedFile && file_exists(Yii::getAlias('@webroot') . $uploadedFile->file)) return $uploadedFile->file;

        return '/files/placeholder-image.jpg';
    }

    public function afterFind()
    {
        parent::afterFind();

        if (!file_exists(Yii::getAlias('@webroot') . $this->file)) $this->delete();
    }

    public function delete()
    {
        if (file_exists(Yii::getAlias('@webroot') . $this->file))
            unlink(Yii::getAlias('@webroot') . $this->file);

        // Clearing cache
        Yii::$app->cache->delete('uploaded-file-' . $this->id);

        return parent::delete();
    }

    private function validatePath($path)
    {
        return (file_exists($path . '/')
            || @mkdir($path . '/', 0777, true)
        );
    }
}