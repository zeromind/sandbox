<?php namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\SluggableBehavior;
use yii\db\ActiveRecord;

use app\components\MultilingualBehavior;
use app\components\MultilingualQuery;

/**
 * Class StaticPage
 * @package app\models
 *
 * @property integer $id
 * @property integer $views
 * @property integer $public
 * @property string $url
 * @property string $title
 * @property string $content
 *
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 */
class StaticPage extends ActiveRecord
{
    public static function tableName()
    {
        return 'static_page';
    }

    public function rules()
    {
        return [
            [['title', 'content'], 'required'],
            [['views', 'public'], 'integer'],
            [['title', 'content', 'url'], 'string'],
            [['views', 'public'], 'default', 'value' => 0]
        ];
    }

    public function attributeLabels()
    {
        return [
            'title'     => Yii::t('app', 'Заголовок'),
            'content'   => Yii::t('app', 'Контент'),
            'url'       => Yii::t('app', 'Ссылка'),
            'views'     => Yii::t('app', 'Просмотры'),
            'public'    => Yii::t('app', 'Опубликовано')
        ];
    }

    public function behaviors()
    {
        return [
            'ml' => [
                'class' => MultilingualBehavior::class,
                'languages' => Languages::getBehaviorsLanguages(),
                'defaultLanguage' => Languages::getCurrentLanguage()->locale,
                'langForeignKey' => 'original_id',
                'tableName' => "{{%static_page_lang}}",
                'attributes' => ['title', 'content']
            ],
            [
                'class' => SluggableBehavior::class,
                'slugAttribute' => 'url',
                'attribute' => 'title',
                'immutable' => true,
                'ensureUnique' => true,
            ],
            [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            TimestampBehavior::class
        ];
    }

    public static function find()
    {
        $q = new MultilingualQuery(get_called_class());
        $q->localized();

        return $q;
    }
}