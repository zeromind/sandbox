<?php namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * Class SettingsLang
 * @package app\models
 *
 * @property integer $id
 * @property integer $original_id
 * @property string $title
 * @property string $description
 * @property string $keywords
 * @property integer $logotype
 */
class SettingsLang extends ActiveRecord
{
    public static function tableName()
    {
        return 'settings_lang';
    }

    public function rules()
    {
        return [
            [['original_id'], 'integer'],
            [['title', 'description', 'keywords'], 'safe'],
            ['logotype', 'safe'],
            ['logotype', 'default', 'value' => 0],
            [['language'], 'string', 'max' => 12]
        ];
    }

    public function attributeLabels()
    {
        return [
            'title'         => Yii::t('app', 'Заголовок'),
            'description'   => Yii::t('app', 'Описание сайта'),
            'keywords'      => Yii::t('app', 'Ключевые слова'),
            'logotype'      => Yii::t('app', 'Логотип'),
            'original_id'   => Yii::t('app', 'ID оригинала'),
            'language'      => Yii::t('app', 'Язык')
        ];
    }
}