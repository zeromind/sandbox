<?php namespace app\models;

use Yii;
use yii\db\ActiveRecord;

class StaticPageLang extends ActiveRecord
{
    public static function tableName()
    {
        return 'static_page_lang';
    }

    public function rules()
    {
        return [
            [['title'], 'string', 'max' => 255],
            [['content'], 'safe'],
            [['original_id'], 'integer'],
            [['language'], 'string', 'max' => 12]
        ];
    }

    public function attributeLabels()
    {
        return [
            'title'         => Yii::t('app', 'Заголовок'),
            'content'       => Yii::t('app', 'Контент'),
            'original_id'   => Yii::t('app', 'ID оригинала'),
            'language'      => Yii::t('app', 'Язык')
        ];
    }
}