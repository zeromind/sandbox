<?php namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * Class Languages
 * @package app\models
 *
 * @property string $name
 * @property string $url
 * @property string $locale
 * @property integer $default
 * @property integer $created_at
 * @property integer $updated_at
 */
class Languages extends \yii\db\ActiveRecord
{
    public static $currentLanguage = null;
    public static $defaultLanguage = null;
    public static $behaviorsList = null;
    public static $behaviorsLanguages = null;


    public static function tableName()
    {
        return '{{%languages}}';
    }

    public function rules()
    {
        return [
            [['name', 'url', 'locale'], 'required'],
            [['name', 'url', 'locale'], 'string'],
            [['default', 'created_at', 'updated_at'], 'integer']
        ];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::class
        ];
    }

    static function setCurrentLanguage($url = null)
    {
        $language = self::getLanguageByUrl($url);
        self::$currentLanguage = ($language === null) ? self::getDefaultLanguage() : $language;
        Yii::$app->language = self::$currentLanguage->locale;
    }

    public static function getCurrentLanguage()
    {
        if (self::$currentLanguage === null)
            self::$currentLanguage = self::getDefaultLanguage();

        return self::$currentLanguage;
    }

    static function getLanguageByUrl($url = null)
    {
        if ($url === null) return null;

        $language = Languages::find()->where('url = :url', [':url' => $url])->one();
        return $language;
    }

    public static function getDefaultLanguage()
    {
        if (self::$defaultLanguage === null)
            self::$defaultLanguage = self::find()->where(['default' => 1])->one();

        return self::$defaultLanguage;
    }

    public static function getBehaviorsLanguages()
    {
        if (self::$behaviorsLanguages === null)
        {
            $behaviors = [];
            $existLanguages = ArrayHelper::map(self::find()->all(), 'locale', 'url');
            foreach ($existLanguages as $locale => $url) {
                $behaviors[$locale] = $url;
            }

            self::$behaviorsLanguages = (count($behaviors)) ? $behaviors : $existLanguages;
        }

        return self::$behaviorsLanguages;
    }

    public static function getBehaviorsList()
    {
        if (self::$behaviorsList === null)
        {
            $behaviors = [];
            $existLanguages = ArrayHelper::map(self::find()->where(['default' => 0])->all(), 'name', 'url');

            foreach ($existLanguages as $name => $url) {
                $behaviors[$name] = $url;
            }

            self::$behaviorsList = (count($behaviors)) ? $behaviors : $existLanguages;
        }

        return self::$behaviorsList;
    }
}