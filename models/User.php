<?php namespace app\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

use borales\extensions\phoneInput\PhoneInputValidator;

/**
 * This is the model class for table "users".
 *
 * @property integer $id
 * @property integer $type
 * @property integer $status
 * @property integer $gender
 * @property integer $image
 * @property string $email
 * @property integer $email_verified
 * @property string $email_confirm_token
 * @property string $phone
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $auth_key
 * @property string $blocked_reason
 * @property string $google_authenticator_secret
 * @property integer $telegram_user_id
 * @property integer $created_at
 * @property integer $updated_at
 *
 * # NAMES
 * @property string $nickname
 * @property string $first_name
 * @property string $second_name
 * @property string $last_name
 *
 * # PSEUDO PROPERTIES
 * @property string $full_name
 * @property array $types
 * @property array $statuses
 * @property string $permission
 * @property string $verification
 * @property string $photoURL
 */
class User extends ActiveRecord implements IdentityInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email'], 'required'],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => self::class,
                'when' => function ($model) { return ($model->isNewRecord || $model->isAttributeChanged('email')); },
                'message' => Yii::t('app', "Данный эмейл уже используется")
            ],
            ['phone', 'unique', 'targetClass' => self::class,
                'when' => function ($model) { return ($model->isNewRecord || $model->isAttributeChanged('phone')); },
                'message' => Yii::t('app', "Данный номер телефона уже используется")
            ],
            [['first_name', 'second_name', 'last_name', 'nickname'], 'string', 'max' => 255],
            [['type', 'status', 'email_verified', 'created_at', 'updated_at', 'telegram_user_id'], 'integer'],
            [['email', 'email_confirm_token', 'password_hash', 'password_reset_token', 'auth_key', 'blocked_reason'], 'string', 'max' => 255],
            [['google_authenticator_secret'], 'string', 'max' => 255],
            [['phone'], 'string', 'max' => 32],
            ['phone', PhoneInputValidator::class],
            ['status', 'in', 'range' => array_keys(self::getStatuses())],
            ['status', 'default', 'value' => 1],
            ['type', 'in', 'range' => array_keys(self::getStatuses())],
            ['type', 'default', 'value' => 0],
            ['gender', 'in', 'range' => array_keys(self::getGenders())],
            ['gender', 'default', 'value' => 0],
            ['image', 'safe'],
            ['image', 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'email'         => Yii::t('app', "Эл. почта"),
            'phone'         => Yii::t('app', "Тел. номер"),
            'status'        => Yii::t('app', "Статус"),
            'type'          => Yii::t('app', "Группа"),
            'gender'        => Yii::t('app', "Пол"),

            'google_authenticator_secret' => Yii::t('app', "Google Authenticator"),

            'first_name'    => Yii::t('app', "Имя"),
            'second_name'   => Yii::t('app', "Отчество"),
            'last_name'     => Yii::t('app', "Фамилия"),
            'nickname'      => Yii::t('app', "Псевдоним"),
        ];
    }

    public function behaviors()
    {
        return [TimestampBehavior::class];
    }

    public static function getStatuses()
    {
        return Yii::$app->params['user_params']['statuses'];
    }

    public static function getTypes()
    {
        return Yii::$app->params['user_params']['types'];
    }

    public static function getGenders()
    {
        return Yii::$app->params['user_params']['genders'];
    }

    public function getStatusName()
    {
        $statuses = self::getStatuses();
        return isset($statuses[$this->status]) ? $statuses[$this->status] : null;
    }

    public function getTypeName()
    {
        $types = self::getTypes();
        return isset($types[$this->type]) ? $types[$this->type] : null;
    }

    public function getFull_Name()
    {
        if (empty($this->first_name) || empty($this->last_name)) return explode('@', $this->email)[0];
        return sprintf("%s %s", $this->last_name, $this->first_name);
    }

    public function getPhotoURL()
    {
        if ($this->image && intval($this->image))
            return UploadedFiles::getCachedFile($this->image);

        return $this->isNewRecord ? '/files/placeholder-image.jpg' : '/files/' . $this->gender . '-placeholder-image.jpg';
    }

    public static function findByEmail($field)
    {
        return \app\modules\user\models\User::findOne(['email' => $field]);
    }

    /**
     * Finds an identity by the given ID.
     * @param string|integer $id the ID to be looked for
     * @return IdentityInterface the identity object that matches the given ID.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }

    /**
     * @param mixed $token
     * @param null $type
     * @return void|IdentityInterface
     * @throws NotSupportedException
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('findIdentityByAccessToken is not implemented.');
    }

    /**
     * Returns an ID that can uniquely identify a user identity.
     * @return string|integer an ID that uniquely identifies a user identity.
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * Returns a key that can be used to check the validity of a given identity ID.
     *
     * The key should be unique for each individual user, and should be persistent
     * so that it can be used to check the validity of the user identity.
     *
     * The space of such keys should be big enough to defeat potential identity attacks.
     *
     * This is required if [[User::enableAutoLogin]] is enabled.
     * @return string a key that is used to check the validity of a given identity ID.
     * @see validateAuthKey()
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function setAuthKey()
    {
        if (is_null($this->auth_key) || empty($this->auth_key))
        {
            $this->auth_key = Yii::$app->security->generateRandomString();
            $this->save(false);
        }
    }

    /**
     * Validates the given auth key.
     *
     * This is required if [[User::enableAutoLogin]] is enabled.
     * @param string $authKey the given auth key
     * @return boolean whether the given auth key is valid.
     * @see getAuthKey()
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * @param $password
     * @throws \yii\base\Exception
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * @param array $permissionRole
     * @return bool
     */
    public function hasPermission($permissionRole = [])
    {
        $roles = array_keys(Yii::$app->authManager->getRolesByUser($this->id));
        return count($roles) !== count(array_diff($roles, $permissionRole));
    }

    /**
     * @return bool
     */
    public function getVerification() : bool
    {
        $verification_expired = (int) Yii::$app->getSession()->get('two-step-expired');
        if ($verification_expired && $verification_expired > time()) return true;
        else Yii::$app->getSession()->set('two-step-expired', 0);

        return false;
    }

    /**
     * @return string
     */
    public function getPermission()
    {
        $roles = Yii::$app->authManager->getRolesByUser($this->id);
        if ($roles && count($roles)) foreach ($roles as $role) return $role->description;
        return Yii::t('app', "Пользователь");
    }

    /**
     * @return \yii\db\ActiveQuery
     * @throws \yii\base\InvalidConfigException
     */
    public function getRoles()
    {
        return $this->hasMany(AuthItem::class, ['name' => 'item_name'])->viaTable('auth_assignment', ['user_id' => 'id']);
    }

    /**
     * @return false|int
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function delete()
    {
        $this->unlinkAll('roles', true);
        $image = UploadedFiles::findOne($this->image);
        if ($image) $image->delete();

        return parent::delete();
    }
}
