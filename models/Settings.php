<?php namespace app\models;

use app\components\MultilingualBehavior;
use app\components\MultilingualQuery;
use Yii;
/**
 * This is the model class for table "settings".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $keywords
 * @property string $head_scripts
 * @property string $body_scripts
 * @property string $end_scripts
 * @property integer $logotype
 *
 * @property integer $maintenance
 * @property integer $system_cache
 * @property string $logotypeURL
 */
class Settings extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'settings';
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['title', 'string', 'max' => 255],
            [['title', 'description', 'keywords'], 'safe'],
            [['head_scripts', 'body_scripts', 'end_scripts'], 'safe'],
            [['maintenance', 'system_cache'], 'integer'],
            ['logotype', 'safe'],
            ['logotype', 'default', 'value' => 0]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'title'         => Yii::t('app', 'Заголовок сайта'),
            'description'   => Yii::t('app', 'Описание сайта'),
            'keywords'      => Yii::t('app', 'Ключевые слова'),

            'head_scripts'  => Yii::t('app', 'Код в шапке сайта'),
            'body_scripts'  => Yii::t('app', 'Код в теле сайта'),
            'end_scripts'   => Yii::t('app', 'Код в подвале сайта'),

            'logotype'      => Yii::t('app', 'Логотип'),

            'maintenance'   => Yii::t('app', "Режим разработчика"),
            'system_cache'  => Yii::t('app', "Кеширование"),
        ];
    }

    public function behaviors()
    {
        return [
            'ml' => [
                'class' => MultilingualBehavior::class,
                'languages' => Languages::getBehaviorsLanguages(),
                'defaultLanguage' => Languages::getCurrentLanguage()->locale,
                'langForeignKey' => 'original_id',
                'tableName' => "{{%settings_lang}}",
                'attributes' => ['title', 'description', 'keywords', 'logotype']
            ]
        ];
    }

    public static function find()
    {
        $q = new MultilingualQuery(get_called_class());
        $q->localized();

        return $q;
    }

    public function getLogotypeURL($lang = 'ru')
    {
        /** @var SettingsLang $settings */
        $settings = SettingsLang::find()->where([
            'AND',
            ['=', 'language', $lang],
            ['=', 'original_id', $this->id]
        ])->one();

        if ($settings && $settings->logotype && intval($settings->logotype)) {
            return UploadedFiles::getCachedFile($settings->logotype);
        }

        return '/files/placeholder-image.jpg';
    }
}
