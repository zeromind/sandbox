<?php namespace app\models;

use Yii;
use yii\db\ActiveRecord;

class MenuLang extends ActiveRecord
{
    public static function tableName()
    {
        return 'menu_lang';
    }

    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
            [['original_id'], 'integer'],
            [['language'], 'string', 'max' => 12]
        ];
    }

    public function attributeLabels()
    {
        return [
            'name'          => Yii::t('app', 'Текст'),
            'original_id'   => Yii::t('app', 'ID оригинала'),
            'language'      => Yii::t('app', 'Язык'),
        ];
    }
}