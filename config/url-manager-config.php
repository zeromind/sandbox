<?php return [
    'enablePrettyUrl' => true,
    'showScriptName' => false,
    'enableStrictParsing' => true,
    'class' => app\components\language\UrlManager::class,
    'rules' => [
        '<_a:error>' => 'frontend/default/<_a>',
        '<_a:(sign-in|sign-out|sign-up)>' => 'user/default/<_a>',
        '<page:[\w\-]+>' => 'frontend/default/index',

        // CONFIRMATIONS
        'confirmation/email' => 'user/default/confirmation-email',
        'confirmation/two-step' => 'user/default/confirmation-two-step',

        // DEFAULT ADMIN
        '<_m:(cp)>/<_c:[\w\-]+>/<_a:[\w\-]+>/<id:\d+>' => '<_m>/<_c>/<_a>',
        '<_m:(cp)>/<_c:[\w\-]+>/<_a:[\w\-]+>' => '<_m>/<_c>/<_a>',
        '<_m:(cp)>/<_c:[\w\-]+>/<id:\d+>' => '<_m>/<_c>/view',
        '<_m:(cp)>/<_c:[\w\-]+>' => '<_m>/<_c>/index',
        '<_m:(cp)>' => '<_m>/dashboard/index',

        // DEFAULT FRONTEND
        '<_m:[\w\-]+>/<_c:[\w\-]+>/<id:\d+>/<_a:[\w\-]+>/<parms:[\w\-]+>' => '<_m>/<_c>/<_a>',
        '<_m:[\w\-]+>/<_c:[\w\-]+>/<id:\d+>/<_a:[\w\-]+>' => '<_m>/<_c>/<_a>',
        '<_m:[\w\-]+>/<_c:[\w\-]+>/<id:\d+>' => '<_m>/<_c>/view',
        '<_m:[\w\-]+>/<_c:[\w\-]+>/<_a:[\w\-]+>/<id:\d+>' => '<_m>/<_c>/<_a>',
        '<_m:[\w\-]+>/<id:\d+>/<_a:[\w\-]+>' => '<_m>/default/<_a>',
        '<_m:[\w\-]+>/<id:\d+>' => '<_m>/default/view',
        '<_m:[\w\-]+>/<_c:[\w\-]+>/<_a:[\w\-]+>' => '<_m>/<_c>/<_a>',
        '<_m:[\w\-]+>/<_c:[\w\-]+>' => '<_m>/<_c>/index',
        '<_m:[\w\-]+>/' => '<_m>/default/index',

        '' => 'frontend/default/index'
    ],
];