<?php return [
    'adminEmail' => 'admin@example.com',
    'bsVersion' => '4.x',
    'bsDependencyEnabled' => false,
    'sourceLanguage' => 'ru-RU',
    'user_params' => [
        'statuses' => [
            1 => Yii::t('app',"Одобрен"),
            0 => Yii::t('app',"Ожидает одобрения"),
            -1 => Yii::t('app',"Заблокирован"),
        ],
        'types' => [
            0 => Yii::t('app',"Стандартный пользователь")
        ],
        'genders' => [
            0 => Yii::t('app',"Парень"),
            1 => Yii::t('app',"Девушка"),
            2 => Yii::t('app',"Трансгендер"),
        ]
    ],
    'switch' => [
        0 => Yii::t('app', "Отключено"),
        1 => Yii::t('app', "Активно")
    ],
    'email' => [
        'sign-up' => 'no-reply@zeromind.space'
    ],
    'allowed' => [
        'ip' => [
            '127.0.0.1',

            // HOME
            '134.249.138.232',

            // WORK
            '195.140.160.193',
            '95.133.161.77'
        ]
    ],
    'allowedIPs' => ['*'],
];
