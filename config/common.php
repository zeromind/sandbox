<?php

use yii\helpers\ArrayHelper;
use yii\i18n\DbMessageSource;

$params = ArrayHelper::merge(
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/local.params.php')
);

return [
    'id' => 'basic',
    'name' => 'Z3R0M1ND',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'language' => 'ru-RU',
    'sourceLanguage' => 'ru-RU',
    'defaultRoute' => 'frontend/default/index',
    'modules' => [
        'cp' => [
            'class' => app\modules\cp\Module::class
        ],
        'frontend' => [
            'class' => app\modules\frontend\Module::class
        ],
        'user' => [
            'class' => app\modules\user\Module::class
        ],
        'api' => [
            'class' => app\modules\api\Module::class
        ],
        'gridview' =>  [
            'class' => kartik\grid\Module::class
        ]
    ],
    'components' => [
        'authManager' => ['class' => 'yii\rbac\DbManager'],
        'view' => [
            'class' => app\components\View::class
        ],
        'request' => [
            'cookieValidationKey' => 'wkxTVJpPd324422zABWWUp15TYnIpc1',
            'class' => app\components\language\Request::class,
            'baseUrl' => '',
            'parsers' => [
                'application/json' => 'yii\web\JsonParser'
            ]
        ],
        'user' => [
            'identityClass' => 'app\modules\user\models\User',
            'enableAutoLogin' => false,
            'loginUrl' => ['sign-in']
        ],
        'i18n' => [
            'translations' => [
                'app*' => [
                    'class' => yii\i18n\DbMessageSource::class,
                    'sourceMessageTable'=>'{{%source_message}}',
                    'messageTable'=>'{{%message}}',
                    'forceTranslation' => true,
                    'sourceLanguage' => 'ru-RU',
                    'enableCaching' => true,
                    'cachingDuration' => 3600
                ],
                'TelegramBot' => [
                    'class' => yii\i18n\DbMessageSource::class,
                    'sourceMessageTable'=>'{{%source_message}}',
                    'messageTable'=>'{{%message}}',
                    'forceTranslation' => true,
                    'sourceLanguage' => 'ru-RU',
                    'enableCaching' => true,
                    'cachingDuration' => 3600
                ]
            ]
        ],
        'formatter' => [
            'class' => 'yii\i18n\Formatter',
            'dateFormat' => 'php:d.m.Y',
            'datetimeFormat' => 'php:d.m.Y H:i:s',
            'timeFormat' => 'php:H:i:s'
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'categories' => ['TelegramBot'],
                    'logFile' => '@runtime/logs/telegram-bot.log'
                ]
            ]
        ],
        'cache' => [
            'class' => 'yii\redis\Cache',
            'redis' => [
                'hostname' => 'localhost',
                'port' => 6379,
                'database' => 0
            ]
        ],
        'urlManager' => require(__DIR__ . '/url-manager-config.php'),
        'errorHandler' => YII_ENV_DEV && YII_DEBUG ? [
            'class' => lucidtaz\yii2whoops\ErrorHandler::class,
        ] : [
            'class' => yii\web\ErrorHandler::class,
            'errorAction' => 'frontend/default/error'
        ]
    ],
    'params' => $params
];
