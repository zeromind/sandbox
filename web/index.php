<?php
defined('YII_DEBUG') or define('YII_DEBUG', false);
defined('YII_ENV_DEV') or define('YII_ENV_DEV', YII_DEBUG);
defined('YII_ENV') or define('YII_ENV', YII_ENV_DEV ? 'dev' : 'prod');

if (YII_DEBUG) {
    error_reporting(E_ALL & E_NOTICE & E_WARNING);
    ini_set('display_errors', 1);
} else {
    error_reporting(E_ERROR);
}

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../vendor/yiisoft/yii2/Yii.php';

$config = yii\helpers\ArrayHelper::merge(
    require(__DIR__ . '/../config/common.php'),
    require(__DIR__ . '/../config/local.common.php')
);

(new yii\web\Application($config))->run();
