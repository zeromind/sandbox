## Z3 | Yii2
#### CONFIGURATION FILES
Make sure that local.*.php files exists in your `app\config` directory.

_local.common.php_
~~~~
<?php
$config = [
    'components' => [
        'db' => require('local.database.php'),
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@app/mail',
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.gmail.com',
                'username' => 'your@gmail.com',
                'password' => 'password',
                'port' => '587',
                'encryption' => 'tls',
                'streamOptions' => [
                    'ssl' => [
                        'allow_self_signed' => true,
                        'verify_peer' => false,
                        'verify_peer_name' => false,
                    ]
                ]
            ]
        ]
    ]
];

if (YII_ENV_DEV && YII_DEBUG)
{
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module'
    ];
}

return $config;

~~~~
_local.database.php_
~~~~
<?php return [
    'class' => 'yii\db\Connection',
    'dsn' => 'pgsql:host=localhost;dbname=database',
    'username' => 'username',
    'password' => 'password',
    'emulatePrepare' => false,
    'enableSchemaCache' => false,
    'schemaCacheDuration' => 3600,
    'schemaCache' => 'cache',
    'charset' => 'utf8'
];

~~~~
_local.params.php_
~~~~
<?php return [
    'localhost' => true, // mark if you work on localhost # TODO
    'baseUrl' => 'https://project.local'
];

~~~~

##### HTML Templates
###### **Porto**
[![Alt text](https://i.imgur.com/wYPgyT2l.jpg)](https://preview.oklerthemes.com/porto/7.3.0/index.html)
###### **Slim**
[![Alt text](https://i.imgur.com/m7rwl7h.jpg)](http://themepixels.me/slim1.1/template/index.html)