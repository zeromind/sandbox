<?php namespace Longman\TelegramBot\Commands\UserCommands;

use Yii;
use Longman\TelegramBot\Commands\UserCommand;
use Longman\TelegramBot\Request;

use app\components\ExchangeRate;
use app\models\Languages;
use app\modules\user\models\User;

class ExchangeCommand extends UserCommand
{
    protected $name = 'exchange';
    protected $description = 'Курсы обмена';
    protected $usage = '/exchange XXX';
    protected $version = '1.0.0.1';

    /**
     * @return \Longman\TelegramBot\Entities\ServerResponse
     * @throws \Longman\TelegramBot\Exception\TelegramException
     * @throws \yii\base\InvalidConfigException
     */
    public function execute()
    {
        $message = $this->getMessage();
        $language = $message->getFrom()->getLanguageCode();
        Languages::setCurrentLanguage($language);

        /** @var \app\models\User $user */
        $user = User::find()->where(['=', 'telegram_user_id', $message->getFrom()->getId()])->one();

        $currency = $message->getText(true);
        $currency = strtoupper($currency);

        if (empty ($currency) == false && in_array($currency, ExchangeRate::CURRENCIES) == false) {
            $text = Yii::t('TelegramBot', "Не верный формат, используйте: {usage}", [
                'usage' => $this->usage
            ]) . "\n" . implode(', ', ExchangeRate::CURRENCIES);
        } elseif ($user && empty($user->id) == false && $user->telegram_user_id == $message->getFrom()->getId()) {
            $exchange = Yii::$app->cache->get('ExchangeRate');
            if ($exchange == false) {
                $exchange = ExchangeRate::getExchangeRate();
            }

            $text = Yii::t('TelegramBot', "Курс обмена на {date}:", [
                    'date' => Yii::$app->formatter->asDatetime($exchange['updated'])
                ]) . "\n\n";

            if (empty($currency)) {
                foreach ($exchange as $_currency => $rate) {
                    if ($_currency != 'updated') {
                        $text .= Yii::t('TelegramBot', "*{currency}* `{buy} {sale} - {base_currency}`", [
                                'currency'      => $_currency,
                                'base_currency' => $rate['currency'],
                                'buy'           => $rate['buy'],
                                'sale'          => $rate['sale']
                            ]) . "\n";
                    }
                }
            } else {
                $text .= Yii::t('TelegramBot', "*{currency}* `{buy} {sale} - {base_currency}`", [
                    'currency'      => $currency,
                    'base_currency' => $exchange[$currency]['currency'],
                    'buy'           => $exchange[$currency]['buy'],
                    'sale'          => $exchange[$currency]['sale']
                ]);
            }
        } else {
            $text = Yii::t('TelegramBot', "Привяжите свой Телеграм к нашей учетной записи на {website}", [
                'website' => Yii::$app->params['baseUrl']
            ]);
        }

        $data = [
            'chat_id' => $message->getChat()->getId(),
            'text'    => $text,
            'parse_mode' => 'MARKDOWN'
        ];

        return Request::sendMessage($data);
    }
}