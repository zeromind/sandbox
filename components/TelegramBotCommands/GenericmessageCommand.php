<?php namespace Longman\TelegramBot\Commands\SystemCommands;

use Yii;
use Longman\TelegramBot\Commands\SystemCommand;
use Longman\TelegramBot\Request;

use app\models\Languages;
use app\modules\user\models\User;

class GenericmessageCommand extends SystemCommand
{
    protected $name = 'generic';
    protected $description = 'Иницализация бота';
    protected $version = '1.0.0.1';

    /**
     * Command execute method
     *
     * @return mixed
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public function execute()
    {
        return parent::execute();
    }
}