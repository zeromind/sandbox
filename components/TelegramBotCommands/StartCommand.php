<?php namespace Longman\TelegramBot\Commands\SystemCommands;

use Longman\TelegramBot\Entities\InlineKeyboard;
use Yii;
use Longman\TelegramBot\Commands\SystemCommand;
use Longman\TelegramBot\Request;

use app\models\Languages;
use app\modules\user\models\User;

class StartCommand extends SystemCommand
{
    protected $name = 'start';
    protected $description = 'Иницализация бота';
    protected $usage = '/start';
    protected $version = '1.0.0.1';

    /**
     * Command execute method
     *
     * @return \Longman\TelegramBot\Entities\ServerResponse
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public function execute()
    {
        $message = $this->getMessage();
        $language = $message->getFrom()->getLanguageCode();
        Languages::setCurrentLanguage($language);

        $data = [
            'chat_id' => $message->getChat()->getId(),
            'text'    => ''
        ];

        /** @var \app\models\User $user */
        $user = User::find()->where(['=', 'telegram_user_id', $message->getFrom()->getId()])->one();
        $keyboard = false;

        if ($user && empty($user->id) == false && $user->telegram_user_id == $message->getFrom()->getId()) {
            $data['text'] = Yii::t('TelegramBot', 'Привет, я пока в разработке, но вот что я могу тебе предложить:');
            $keyboard = new InlineKeyboard([
                [
                    'text'          => Yii::t('TelegramBot', "Валютный курс"),
                    'callback_data' => 'exchange'
                ]
            ]);
        } else {
            $data['text'] = Yii::t('TelegramBot', "Привяжите свой Телеграм к учетной записи на {website}", [
                'website' => Yii::$app->params['baseUrl']
            ]);
        }

        if ($keyboard) $data['reply_markup'] = $keyboard;

        return Request::sendMessage($data);
    }
}