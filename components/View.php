<?php namespace app\components;

/**
 * Class View
 * @package app\components
 *
 * @property string $head_scripts
 * @property string $body_scripts
 * @property string $end_scripts
 */
class View extends \yii\web\View
{
    public $head_scripts;
    public $body_scripts;
    public $end_scripts;

    protected function renderHeadHtml()
    {
        $content = "\n\t<!-- head_scripts -->\n" . $this->head_scripts . "\n";

        return parent::renderHeadHtml() . $content;
    }

    protected function renderBodyBeginHtml()
    {
        $content = "\n\t<!-- body_scripts -->\n" . $this->body_scripts . "\n";

        return parent::renderBodyBeginHtml() . $content;
    }

    protected function renderBodyEndHtml($ajaxMode)
    {
        $content = "\n\t<!-- end_scripts -->\n" . $this->end_scripts . "\n";

        return parent::renderBodyEndHtml($ajaxMode) . $content;
    }
}