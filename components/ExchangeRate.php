<?php namespace app\components;

use Yii;

class ExchangeRate
{
    CONST URL = 'https://api.privatbank.ua/p24api/pubinfo?exchange&coursid=11';
    CONST CURRENCIES = ['EUR', 'USD', 'RUB', 'BTC'];

    public static function getExchangeRate()
    {
        $rates = [];

        $xml = @file_get_contents(self::URL);
        if ($xml && $data = simplexml_load_string($xml)) foreach ($data->row as $rate) {
            $rate = $rate->exchangerate;
            $currency = (string) $rate['ccy'];

            $rates[$currency] = [
                'buy'       => (string) $rate['buy'],
                'sale'      => (string) $rate['sale'],
                'currency'  => (string) $rate['base_ccy']
            ];
        }

        $rates['updated'] = time();
        Yii::$app->cache->set('ExchangeRate', $rates, 3600);

        return $rates;
    }
}