<?php namespace app\components;

use Yii;
use yii\base\Exception;

use app\models\Settings;

class ConsoleController extends \yii\console\Controller
{
    public $settings = [];
    public $system_cache = null;

    /**
     * @throws Exception
     */
    public function init()
    {
        $this->settings = self::getSettings();
        $this->system_cache = $this->settings->system_cache;

        Yii::setAlias('@baseUrl', Yii::$app->params['baseUrl']);
        Yii::setAlias('@frontend', '@app/modules/frontend/views');
        Yii::setAlias('@webroot', __DIR__ . '/../web');
        Yii::setAlias('@web', '/');

        parent::init();
    }

    /**
     * @return Settings|mixed|null
     * @throws Exception
     */
    public static function getSettings()
    {
        $settings = Yii::$app->cache->get('app/settings');
        if ($settings) return $settings;
        else
        {
            $settings = Settings::findOne(1);
            if (!$settings) {
                $settings = Settings::find()->one();
                if (!$settings) throw new Exception(Yii::t('app', "Не удалось получить конфигурацию."), 422);
            }
            else Yii::$app->cache->set('app/settings', $settings);
        }

        return $settings;
    }
}