<?php namespace app\components;

use app\models\Languages;
use Yii;
use yii\base\Exception;

use app\models\Settings;

/**
 * Class Controller
 * @package app\components
 *
 * @property \app\components\View $view
 * @property \app\models\Settings $settings
 * @property integer $system_cache
 */
class Controller extends \yii\web\Controller
{
    public $settings = [];
    public static $uip = null;

    public $_seo_description = null;
    public $_seo_keywords = [];

    public $system_cache = null;

    public $languages = false;
    public $oldLangAssoc = [];
    public $newLangAssoc = [];

    /**
     * @throws Exception
     */
    public function init()
    {
        self::getUserIP();

        $this->settings = self::getSettings();
        $this->system_cache = $this->settings->system_cache;
        $this->getLanguages();

        // DEFAULT SEO
        $this->setSEO('description', $this->settings->description, true);
        $this->setSEO('keywords', $this->settings->keywords, true);

        Yii::setAlias('@baseUrl', Yii::$app->params['baseUrl']);
        Yii::setAlias('@frontend', '@app/modules/frontend/views');
        Yii::setAlias('@webroot', __DIR__ . '/../web');
        Yii::setAlias('@web', '/');

        parent::init();
    }

    /**
     * @return Settings|mixed|null
     * @throws Exception
     */
    public static function getSettings()
    {
        $settings = Yii::$app->cache->get('app/settings');
        if ($settings) return $settings;
        else
        {
            $settings = Settings::findOne(1);
            if (!$settings) {
                $settings = Settings::find()->one();
                if (!$settings) throw new Exception(Yii::t('app', "Не удалось получить конфигурацию."), 422);
            }
            else Yii::$app->cache->set('app/settings', $settings);
        }

        return $settings;
    }

    public static function getUserIP() : void
    {
        if (Yii::$app->user->identity && array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
            $str = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            self::$uip = $str[count($str) - 1];
        } else {
            self::$uip = $_SERVER['REMOTE_ADDR'];
        }
    }

    /**
     * @param bool|string $ip
     * @return bool
     */
    public static function hasAccess($ip = false) : bool
    {
        if (!Yii::$app->user->isGuest) {
            return Yii::$app->user->identity->hasPermission(['admin', 'moderator']);
        } elseif ($ip) {
            return in_array($ip, Yii::$app->params['allowed']['ip']);
        }

        return in_array(self::$uip, Yii::$app->params['allowed']['ip']);
    }

    /**
     * @param string $action_id
     * @throws \yii\web\BadRequestHttpException
     */
    public function checkMaintenanceMode(string $action_id) : void
    {
        if ($this->settings->maintenance && self::hasAccess() == false && (!isset ($this->allowedDirections) || !in_array($action_id, $this->allowedDirections)))
        {
            $this->layout = '/maintenance';
            $action = $this->createAction($action_id);
            $this->beforeAction($action);
        }
    }

    /**
     * @param string $action_id
     * @throws \Throwable
     * @throws \yii\web\BadRequestHttpException
     */
    public function checkBlocked(string $action_id) : void
    {
        if (!Yii::$app->user->isGuest)
        {
            /** @var \app\models\User $user */
            $user = Yii::$app->user->getIdentity();
            if ($user->status === -1)
            {
                $this->layout = '/blocked';
                $action = $this->createAction($action_id);
                $this->beforeAction($action);
            }
        }
    }

    // TODO: Advanced SEO

    public function setTitle(string $title) : void
    {
        if (is_string($title)) $this->view->title .= " » " . $title;
        elseif (is_array($title)) foreach ($title as $subtitle) $this->view->title .= " » " . $subtitle;
    }

    public function setSEO($key, $content, $reset = false) : void
    {
        switch ($key)
        {
            case 'keywords':
                {
                    if ($reset) $this->_seo_keywords = [];

                    if (is_array($content)) foreach ($content as $keyword)
                        $this->_seo_keywords[] = $keyword;
                    elseif (is_string($content))
                        $this->_seo_keywords[] = $content;
                } break;

            case 'description':
                {
                    if ($reset) $this->_seo_description = null;

                    if (is_array($content)) foreach ($content as $description)
                        $this->_seo_description .= $description . " ";
                    elseif (is_string($content))
                        $this->_seo_description .= $content . " ";
                } break;
        }
    }

    public static function getOldLangAssoc($lang)
    {
        $languages = Yii::$app->controller->oldLangAssoc;
        return isset($languages[$lang])? $languages[$lang] : 'en';
    }

    public static function getNewLangAssoc($lang)
    {
        $languages = Yii::$app->controller->newLangAssoc;
        return isset($languages[$lang])? $languages[$lang] : 'en';
    }

    public function getLanguages()
    {
        if ($this->system_cache == false) {
            $this->languages = false;
        } else {
            $this->languages = Yii::$app->cache->get('languages_' . Yii::$app->language);
        }

        if ($this->languages === false) {
            $this->languages = Languages::find()->indexBy('url')->all();
            Yii::$app->cache->set('languages_' . Yii::$app->language, $this->languages);
        }

        /** @var Languages $language */
        foreach ($this->languages as $language) {
            $this->oldLangAssoc[$language->locale] = $language->url;
            $this->newLangAssoc[$language->url] = $language->locale;
        }
    }

    /**
     * @param string $id
     * @param array $params
     * @return mixed
     * @throws \Throwable
     * @throws \yii\base\InvalidRouteException
     * @throws \yii\web\BadRequestHttpException
     */
    public function runAction($id, $params = [])
    {
        $this->view->title = $this->settings->title;
        $this->view->registerMetaTag(['name' => 'keywords', 'content' => implode(",", $this->_seo_keywords)], 'keywords');
        $this->view->registerMetaTag(['name' => 'description', 'content' => trim($this->_seo_description)], 'description');

        $this->view->head_scripts = $this->settings->head_scripts;
        $this->view->body_scripts = $this->settings->body_scripts;
        $this->view->end_scripts = $this->settings->end_scripts;

        $this->checkMaintenanceMode($id);
        $this->checkBlocked($id);

        return parent::runAction($id, $params);
    }
}