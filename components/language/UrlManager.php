<?php namespace app\components\language;

use app\models\Languages;

class UrlManager extends \yii\web\UrlManager
{
    public function createUrl($params)
    {
        if (isset($params['lang_id']))
        {
            $language = Languages::findOne($params['lang_id']);
            if ($language === null) $language = Languages::getDefaultLanguage();
            unset($params['lang_id']);
        }
        else
        {
            $language = Languages::getCurrentLanguage();
            $default_language = Languages::getDefaultLanguage();
        }

        $url = parent::createUrl($params);

        if (isset ($default_language) && $default_language->url !== $language->url)
        {
            if ($url == '/') return '/' . $language->url . '/';

            return '/' . $language->url . $url;
        }

        return $url;
    }
}