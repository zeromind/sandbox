<?php namespace app\components\widgets;

class Languages extends \yii\bootstrap4\Widget
{
    public function run()
    {
        return $this->render('languages', [
            'current' => \app\models\Languages::getCurrentLanguage(),
            'behaviors' => \app\models\Languages::getBehaviorsLanguages()
        ]);
    }
}