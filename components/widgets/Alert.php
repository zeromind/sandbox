<?php namespace app\components\widgets;

use yii\helpers\Html;

class Alert extends \yii\bootstrap4\Widget
{
    public $alertTypes = [
        'error'   => 'alert alert-solid alert-error',
        'danger'  => 'alert alert-solid alert-danger',
        'success' => 'alert alert-solid alert-success',
        'info'    => 'alert alert-solid alert-info',
        'warning' => 'alert alert-solid alert-warning'
    ];

    public function init()
    {
        parent::init();

        $session = \Yii::$app->getSession();

        foreach ($session->getAllFlashes() as $type => $data)
        {
            if (isset($this->alertTypes[$type]))
            {
                foreach ((array) $data as $i => $message)
                {
                    $this->options['class'] = $this->alertTypes[$type];
                    $this->options['id'] = $this->getId() . '-' . $type . '-' . $i;

                    $message .= Html::button('<span aria-hidden="true">×</span>', [
                        'class' => "close",
                        'data-dismiss' => "alert",
                        'aria-label' => "Close"
                    ]);

                    echo \yii\bootstrap4\Alert::widget([
                        'closeButton' => false,
                        'body' => $message,
                        'options' => $this->options
                    ]);
                }

                $session->removeFlash($type);
            }
        }
    }
}