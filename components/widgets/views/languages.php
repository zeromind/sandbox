<?php
/** @var \app\models\Languages $current */
/** @var array $behaviors */
?>
<nav class="header-nav-top order-1 d-inline-flex m-l-15">
    <a href="javascript:void();" class="header-nav-features-toggle" role="button" id="dropdownLanguage" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <img src="/images/flags/flag_<?= $current->url; ?>.svg" class="img-fluid rounded-circle p-1 bg-dark" width="35" style="border: 2px solid rgba(255,255,255, .15);" />
    </a>
    <div class="dropdown-menu language-selector bg-color-dark-scale-2 p-0" aria-labelledby="dropdownLanguage">
        <?php foreach ($behaviors as $language => $code) : $url = '/' . $code . Yii::$app->getRequest()->getLangUrl(); if ($code == $current->url) continue; ?>
            <a class="dropdown-item p-3" href="<?= \yii\helpers\Url::to($url); ?>">
                <img src="/images/flags/flag_<?= $code; ?>.svg" class="img-fluid rounded-circle" width="25" />
            </a>
        <?php endforeach; ?>
    </div>
</nav>