<?php namespace app\components;

use Yii;
use yii\web\Response;
use yii\web\HttpException;

class RestFul extends \yii\rest\ActiveController
{
    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        parent::init();
    }
}