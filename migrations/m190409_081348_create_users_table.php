<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%users}}`.
 */
class m190409_081348_create_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%users}}', [
            'id' => $this->primaryKey(),
            'type' => $this->integer(32)->notNull()->defaultValue(0),
            'status' => $this->integer(32)->notNull()->defaultValue(0),
            'gender' => $this->integer(32)->defaultValue(0),
            'image' => $this->integer(32)->defaultValue(0),
            'first_name' => $this->string(64),
            'second_name' => $this->string(64),
            'last_name' => $this->string(64),
            'nickname' => $this->string(64),
            'email' => $this->string(512),
            'email_verified' => $this->integer(1)->notNull()->defaultValue(0),
            'email_confirm_token' => $this->string(255),
            'phone' => $this->string(64),
            'password_hash' => $this->string(255),
            'password_reset_token' => $this->string(255),
            'auth_key' => $this->string(255),
            'blocked_reason' => $this->string(255),
            'google_authenticator_secret' => $this->string(255),
            'telegram_user_id' => $this->integer(32),
            'created_at' => $this->integer(32)->notNull()->defaultValue(0),
            'updated_at' => $this->integer(32)->notNull()->defaultValue(0)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%users}}');
    }
}
