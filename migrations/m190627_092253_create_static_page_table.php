<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%static_page}}`.
 */
class m190627_092253_create_static_page_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%static_page}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255)->notNull(),
            'content' => $this->text()->notNull(),
            'url' => $this->string(255)->notNull(),
            'views' => $this->integer(16)->notNull()->defaultValue(9),
            'public' => $this->tinyInteger(1)->notNull()->defaultValue(0),
            'created_at' => $this->integer(32)->notNull()->defaultValue(0),
            'updated_at' => $this->integer(32)->notNull()->defaultValue(0),
            'created_by' => $this->integer(32)->notNull()->defaultValue(0),
            'updated_by' => $this->integer(32)->notNull()->defaultValue(0)
        ]);

        $this->createIndex('idx-created_by', 'static_page', 'created_by');
        $this->addForeignKey('fk-created_by', 'static_page', 'created_by', 'users', 'id');

        $this->createIndex('idx-updated_by', 'static_page', 'updated_by');
        $this->addForeignKey('fk-updated_by', 'static_page', 'updated_by', 'users', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%static_page}}');
    }
}
