<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%uploaded_files}}`.
 */
class m190321_152632_create_uploaded_files_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%uploaded_files}}', [
            'id' => $this->primaryKey(),
            'file' => $this->string(512)->notNull(),
            'created_at' => $this->integer(32)->notNull()->defaultValue(0),
            'updated_at' => $this->integer(32)->notNull()->defaultValue(0)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%uploaded_files}}');
    }
}
