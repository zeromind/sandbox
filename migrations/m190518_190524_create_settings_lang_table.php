<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%settings_lang}}`.
 */
class m190518_190524_create_settings_lang_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%settings_lang}}', [
            'id' => $this->primaryKey(),
            'original_id' => $this->integer()->notNull(),
            'title' => $this->string(512),
            'description' => $this->string(2000),
            'keywords' => $this->string(2000),
            'logotype' => $this->integer(32)->notNull(),
            'language' => $this->string(12)->notNull()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%settings_lang}}');
    }
}
