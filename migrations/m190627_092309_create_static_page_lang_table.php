<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%static_page_lang}}`.
 */
class m190627_092309_create_static_page_lang_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%static_page_lang}}', [
            'id' => $this->primaryKey(),
            'original_id' => $this->integer()->notNull(),
            'title' => $this->string(255),
            'content' => $this->text(),
            'language' => $this->string(12)->notNull()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%static_page_lang}}');
    }
}
