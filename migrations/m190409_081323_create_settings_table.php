<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%settings}}`.
 */
class m190409_081323_create_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%settings}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255)->notNull(),
            'description' => $this->text(),
            'keywords' => $this->text(),
            'head_scripts' => $this->text(),
            'body_scripts' => $this->text(),
            'end_scripts' => $this->text(),
            'maintenance' => $this->integer(1)->notNull()->defaultValue(1),
            'system_cache' => $this->integer(1)->notNull()->defaultValue(1),
            'logotype' => $this->integer(32)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%settings}}');
    }
}
