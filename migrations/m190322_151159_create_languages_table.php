<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%languages}}`.
 */
class m190322_151159_create_languages_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%languages}}', [
            'id' => $this->primaryKey(),
            'url' => $this->string(6)->notNull(),
            'name' => $this->string(32)->notNull(),
            'locale' => $this->string(6)->notNull(),
            'default' => $this->integer(1)->notNull()->defaultValue(0),
            'created_at' => $this->integer(32)->notNull()->defaultValue(0),
            'updated_at' => $this->integer(32)->notNull()->defaultValue(0),
        ]);

        $this->insert('{{%languages}}', [
            'url' => 'ru',
            'name' => 'Русский',
            'locale' => 'ru_RU',
            'default' => 1,
            'created_at' => time(),
            'updated_at' => time()
        ]);

        $this->insert('{{%languages}}', [
            'url' => 'en',
            'name' => 'English',
            'locale' => 'en_US',
            'default' => 0,
            'created_at' => time(),
            'updated_at' => time()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%languages}}');
    }
}
