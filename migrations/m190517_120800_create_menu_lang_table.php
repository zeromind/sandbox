<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%menu_lang}}`.
 */
class m190517_120800_create_menu_lang_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%menu_lang}}', [
            'id' => $this->primaryKey(),
            'original_id' => $this->integer()->notNull(),
            'name' => $this->string(255)->notNull(),
            'language' => $this->string(12)->notNull()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%menu_lang}}');
    }
}
