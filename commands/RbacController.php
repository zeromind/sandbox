<?php namespace app\commands;

use Yii;

class RbacController extends \app\components\ConsoleController
{
    public function actionInit() {
        $auth = Yii::$app->authManager;
        $auth->removeAll();

        $admin = $auth->createRole('admin');
        $auth->add($admin);

        $moderator = $auth->createRole('moderator');
        $auth->add($moderator);

        $root = $auth->createPermission('root');
        $root->description = 'Просмотр админки';
        $auth->add($root);

        $auth->addChild($admin, $moderator);
        $auth->addChild($admin, $root);

        $auth->assign($admin, 1);
    }
}