<?php namespace app\modules\api;

use Yii;
use app\models\Languages as Lang;

/**
 * Class Module
 * @package app\modules\api
 *
 * @property array $data
 */
class Module extends \app\components\BaseModule
{
    public $controllerNamespace = 'app\modules\api\controllers';

    public $lang, $salt, $data;

    public function init()
    {
        $this->salt = Yii::$app->params['salt'];

        $headers = Yii::$app->request->headers;
        if ($headers->has('Accept-Language')) {
            $this->lang = $headers->get('Accept-Language');
            Lang::setCurrentLanguage($this->lang);
        }

        parent::init();
    }

    public function sendResponse($status = 'success', $code = 200, $message = false, $key = false)
    {
        $content = [];
        $content['response']['status'] = $status;
        $content['response']['data'] = $this->data;

        if ($status == 'error') {
            $content['error']['code'] = $code;

            if ($key) {
                $content['error']['message'][$key] = $message;
            } else {
                $content['error']['message'] = $message;
            }

            $content['response']['data'] = null;
        }

        Yii::$app->response->statusCode = $code;
        Yii::$app->response->content = json_encode($content, JSON_NUMERIC_CHECK | JSON_UNESCAPED_UNICODE);
        Yii::$app->response->send();
        exit;
    }
}
