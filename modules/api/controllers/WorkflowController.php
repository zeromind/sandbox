<?php namespace app\modules\api\controllers;

use yii\filters\AccessControl;
use yii\filters\VerbFilter;

class WorkflowController extends BaseController
{
    public $modelClass = 'app\modules\api\models\RestFul';

    const DOOR_URI = 'http://doorlock.ucg.io/';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['time'],
                        'allow' => true
                    ]
                ]
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'time'  => ['GET'],
                ]
            ]
        ];
    }

    public function actionTime()
    {
        $url = self::DOOR_URI . '?' . http_build_query(['day' => date("Y-m-d")]);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_VERBOSE, true);
        curl_setopt($ch , CURLOPT_COOKIE, '_identity=0ff701747ef96e7f36014783f97631becf283d762e093056ea92760baefdfe12a%3A2%3A%7Bi%3A0%3Bs%3A9%3A%22_identity%22%3Bi%3A1%3Bs%3A54%3A%22%5B108342478%2C%22ZKDsyMBlllD_nbHYDjl87HzBQtOXESCa%22%2C2592000%5D%22%3B%7D; _csrf=332bd7d98dfb6f7b319f080239d2451561a7299aab3451b609465a1ca361fb70a%3A2%3A%7Bi%3A0%3Bs%3A5%3A%22_csrf%22%3Bi%3A1%3Bs%3A32%3A%22hqJwbUhtwZh5TDANHkBulpFkcpyXWQ3s%22%3B%7D; PHPSESSID=cppu9istrkjva7m7uccq7kvq87');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $response = curl_exec($ch);
        curl_close($ch);


        return $response;
    }
}