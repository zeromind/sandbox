<?php namespace app\modules\api\controllers;

use Longman\TelegramBot\Telegram;
use Longman\TelegramBot\TelegramLog;
use Longman\TelegramBot\Exception\TelegramException;

use Yii;
use yii\rest\ActiveController;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

use app\modules\api\Module;

/**
 * Class TelegramController
 * @package app\modules\api\controllers
 *
 * @property Module $module;
 */
class TelegramController extends ActiveController
{
    public $modelClass = 'app\modules\api\models\RestFul';
    public $telegram_ip_range = [
        ['lower' => '149.154.160.0', 'upper' => '149.154.175.255'],
        ['lower' => '91.108.4.0',    'upper' => '91.108.7.255'],
        ['lower' => '195.140.160.0', 'upper' => '195.140.160.255']
    ];

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['webhook', 'delete-webhook', 'set-webhook'],
                        'allow' => true
                    ]
                ]
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'webhook'  => ['GET', 'POST', 'PUT'],
                ]
            ]
        ];
    }

    /**
     * @param $action
     * @return bool
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        $ip = $_SERVER['REMOTE_ADDR'];
        $ip_dec = (float) sprintf("%u", ip2long($ip));
        $access = false;

        foreach ($this->telegram_ip_range as $telegram_ip_range) if ($access == false) {
            $lower_dec = (float) sprintf("%u", ip2long($telegram_ip_range['lower']));
            $upper_dec = (float) sprintf("%u", ip2long($telegram_ip_range['upper']));

            if ($ip_dec >= $lower_dec and $ip_dec <= $upper_dec) {
                $access = true;
            }
        }

        if ($access == false) {
            $this->module->sendResponse('error', 403, $ip . ' not whitelisted.');
        }

        return parent::beforeAction($action);
    }

    public function actionSetWebhook()
    {
        $url = Yii::$app->params['baseUrl'] . '/api/telegram/webhook?token=' . Yii::$app->params['telegram']['token'];
        try {
            $telegram = new Telegram(Yii::$app->params['telegram']['token'], Yii::$app->params['telegram']['name']);
            $response = $telegram->setWebhook($url);
            if ($response->isOk()) {
                Yii::info('Webhook "' . $url . '" added successfully.', 'TelegramBot');

                $response = (array) $response->getRawData();
                $response['response']['url'] = $url;

                $this->module->data = $response;
                $this->module->sendResponse();
            } else {
                Yii::error('Webhook "' . $url . '" not added.', 'TelegramBot');

                $this->module->sendResponse('error', 422, "Can't add webhook");
            }
        } catch (TelegramException $e) {
            TelegramLog::error($e);
            Yii::error($e->getMessage(), 'TelegramBot Exception');
        }
    }

    public function actionDeleteWebhook()
    {
        try {
            $telegram = new Telegram(Yii::$app->params['telegram']['token'], Yii::$app->params['telegram']['name']);
            $response = $telegram->deleteWebhook();
            if ($response->isOk()) {
                Yii::info('Webhook removed successfully.', 'TelegramBot');

                $this->module->data = (array) $response->getRawData();
                $this->module->sendResponse();
            } else {
                Yii::error('Webhook not removed.', 'TelegramBot');

                $this->module->sendResponse('error', 422, "Webhook can't be deleted!");
            }
        } catch (TelegramException $e) {
            TelegramLog::error($e);
            Yii::error($e->getMessage(), 'TelegramBot Exception');
        }
    }

    /**
     * @param string $token - TelegramBot token validation
     */
    public function actionWebhook($token)
    {
        if ($token == Yii::$app->params['telegram']['token']) {
            try {
                $telegram = new Telegram(Yii::$app->params['telegram']['token'], Yii::$app->params['telegram']['name']);
                $telegram->addCommandsPaths([
                    Yii::getAlias('@app/components/TelegramBotCommands/')
                ]);

                $telegram->enableLimiter();
                $telegram->handle();
            } catch (TelegramException $e) {
                Yii::error($e->getMessage(), 'TelegramBot Exception');
            }
        } else {
            Yii::error('Token validation failed. Not match.', 'TelegramBot');
        }
    }
}