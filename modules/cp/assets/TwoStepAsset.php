<?php namespace app\modules\cp\assets;

use yii\web\AssetBundle;

class TwoStepAsset extends AssetBundle
{
    public $basePath = '@webroot/modules/cp/assets';
    public $baseUrl = '@web/slimadmin-11/template/app';

    public $css = [
        'lib/font-awesome/css/font-awesome.css',
        'lib/Ionicons/css/ionicons.css',
        'css/slim.css'
    ];

    public $js = [
        'lib/jquery.cookie/js/jquery.cookie.js',
        'lib/jquery.mask/jquery.mask.min.js',
        'js/slim.js'
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap4\BootstrapAsset',
        'yii\bootstrap4\BootstrapPluginAsset',
    ];
}