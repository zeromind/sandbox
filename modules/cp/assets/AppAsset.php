<?php namespace app\modules\cp\assets;

use Yii;
use yii\web\AssetBundle;

class AppAsset extends AssetBundle
{
    public $basePath = '@webroot/modules/cp/assets';
    public $baseUrl = '@web/slimadmin-11/template/app';

    public $css = [
        'lib/font-awesome/css/font-awesome.css',
        'https://use.fontawesome.com/releases/v5.8.1/css/all.css',
        'lib/Ionicons/css/ionicons.css',
        'lib/chartist/css/chartist.css',
        'lib/rickshaw/css/rickshaw.min.css',
        'lib/perfect-scrollbar/css/perfect-scrollbar.min.css',
        'lib/jquery-confirm/jquery-confirm.min.css',
        'css/slim.min.css',
        'css/slim.header-two.css',
        'css/util.css',
        '/css/cp-custom.css'
    ];

    public $js = [
        'lib/jquery.cookie/js/jquery.cookie.js',
        'lib/chartist/js/chartist.js',
//        'lib/rickshaw/js/rickshaw.min.js',
        'lib/d3/js/d3.js',
        'lib/jquery.sparkline.bower/js/jquery.sparkline.min.js',
        'lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js',
        'lib/jquery-confirm/jquery-confirm.min.js',
        'lib/jquery.mask/jquery.mask.min.js',
        'js/ResizeSensor.js',
        'js/dashboard.js',
        'js/slim.js'
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap4\BootstrapAsset',
        'yii\bootstrap4\BootstrapPluginAsset'
    ];
}