<?php namespace app\modules\cp\controllers;

use app\modules\cp\models\StaticPage;
use app\modules\cp\models\StaticPageSearch;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

use app\components\Controller;
use yii\web\Response;

class StaticController extends Controller
{
    public $layout = "./sidebar";

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update', 'delete'],
                        'allow' => true,
                        'roles' => ['admin', 'moderator'],
                    ]
                ]
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'index' => ['GET']
                ]
            ]
        ];
    }

    public function sidebarSettings() : array
    {
        return [
            'visibility' => true,
            'position' => 12,
            'actions' => [],
            'title' => Yii::t('app', "Статические страницы"),
            'icon' => 'icon ion-ios-book'
        ];
    }

    public function actionIndex() : string
    {
        $searchModel = new StaticPageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $model = new StaticPage();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->getSession()->setFlash('success', Yii::t('app', "Страница успешно добавлена"));
            return $this->redirect(['update', 'id' => $model->id]);
        }

        return $this->render('crud/create', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->getSession()->setFlash('success', Yii::t('app', "Страница «<b>{title}</b>» успешно сохранена", ['title' => $model->title]));
            return $this->redirect(['update', 'id' => $model->id]);
        }

        return $this->render('crud/update', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id) : Response
    {
        $model = $this->findModel($id);
        $title = $model->title;

        if ($model->delete()) Yii::$app->getSession()->setFlash('success', Yii::t('app', "Страница «<b>{title}</b>» успешно удалена", ['title' => $title]));
        else Yii::$app->getSession()->setFlash('warning', Yii::t('app', "Вы не можете удалить данную страницу"));

        return $this->redirect(['index']);
    }

    /**
     * Finds the Lang model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     * @return StaticPage the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = StaticPage::findOne($id)) !== null) return $model;
        else throw new NotFoundHttpException(Yii::t('app', 'Страница не найдена'));
    }
}