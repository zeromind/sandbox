<?php namespace app\modules\cp\controllers;

use app\modules\cp\models\MenuSearch;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

use app\components\Controller;

use app\models\Menu;

class MenuController extends Controller
{
    public $layout = "./sidebar";

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update', 'delete'],
                        'allow' => true,
                        'roles' => ['admin', 'moderator'],
                    ]
                ]
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'index' => ['GET']
                ]
            ]
        ];
    }

    public function sidebarSettings() : array
    {
        return [
            'visibility' => true,
            'position' => 11,
            'actions' => [],
            'title' => Yii::t('app', "Меню"),
            'icon' => 'icon ion-android-menu'
        ];
    }

    public function actionIndex() : string
    {
        $searchModel = new MenuSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionCreate()
    {
        $model = new Menu();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Menu::flushCache();
            return $this->redirect([
                'update', 'id' => $model->id
            ]);
        } else {
            return $this->render('crud/create', [
                'model' => $model
            ]);
        }
    }

    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if($model->load(Yii::$app->request->post()) && $model->save()) {
            Menu::flushCache();
            Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Сохранено'));

            return $this->redirect([
                'update', 'id' => $model->id
            ]);
        } else {
            return $this->render('crud/update', [
                'model' => $model
            ]);
        }
    }

    public function actionDelete($id) {
        $this->findModel($id)->delete();

        Menu::flushCache();
        return $this->redirect(['index']);
    }

    /**
     * Finds the Menu model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     * @param boolean $ml
     *
     * @return Menu the loaded model
     *
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id, $ml = true)
    {
        if ($ml) {
            $model = Menu::find()->where('id = :id', [':id' => $id])->multilingual()->one();
        } else {
            $model = Menu::findOne($id);
        }

        if ($model == null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        return $model;
    }
}