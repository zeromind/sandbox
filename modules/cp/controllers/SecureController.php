<?php namespace app\modules\cp\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

use app\components\Controller;
use app\modules\cp\models\TwoStepVerification;

class SecureController extends Controller
{
    public $layout = "./sidebar";

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['two-step'],
                        'allow' => true,
                        'roles' => ['admin', 'moderator']
                    ]
                ]
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'two-step' => ['GET', 'POST']
                ]
            ]
        ];
    }

    public function sidebarSettings()
    {
        return [
            'visibility' => false
        ];
    }

    public function actionTwoStep()
    {
        $model = new TwoStepVerification();
        if ($model->load(Yii::$app->request->post()) && $model->verify())
        {
            Yii::$app->getSession()->setFlash('success', Yii::t('app', "Вы успешно подтвердили <b>Google Authenticator</b>"));
            Yii::$app->getSession()->set('two-step-expired', time() + 86400);
            return $this->goBack(['/cp/dashboard/index']);
        }
        else return $this->renderPartial('two-step', [
            'model' => $model
        ]);

        return $this->renderPartial('two-step', []);
    }
}