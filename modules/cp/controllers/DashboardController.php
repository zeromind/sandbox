<?php namespace app\modules\cp\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

use app\components\Controller;

class DashboardController extends Controller
{
    public $layout = "./sidebar";

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['admin', 'moderator'],
                    ]
                ]
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'index' => ['GET']
                ]
            ]
        ];
    }

    public function sidebarSettings()
    {
        return [
            'visibility' => true,
            'position' => 0,
            'actions' => [],
            'title' => Yii::t('app', "Панель управления"),
            'icon' => 'icon ion-stats-bars'
        ];
    }

    public function actionIndex()
    {
        return $this->render('index', []);
    }
}