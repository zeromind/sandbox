<?php namespace app\modules\cp\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;

use app\components\Controller;
use app\components\GoogleAuthenticator;

use app\modules\cp\components\FileUpload;
use app\modules\cp\models\User;
use app\modules\cp\models\UsersSearch;

class UsersController extends Controller
{
    public $layout = "./sidebar";

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update', 'delete'],
                        'allow' => true,
                        'roles' => ['admin', 'moderator'],
                    ]
                ]
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'index' => ['GET']
                ]
            ]
        ];
    }

    public function sidebarSettings() : array
    {
        return [
            'visibility' => true,
            'position' => 10,
            'actions' => [],
            'title' => Yii::t('app', "Пользователи"),
            'icon' => 'icon ion-person-stalker'
        ];
    }

    public function actionIndex() : string
    {
        $searchModel = new UsersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $model = new User();

        if ($model->load(Yii::$app->request->post()))
        {
            $image = UploadedFile::getInstance($model, 'image');
            if ($image && !is_null($image))
            {
                $image = new FileUpload($image, $model, 'image', '/files/uploads/users/photos/');
                $model = $image->saveAs();
            }
            else $model->image = $model->getOldAttribute('image');

            if ($model->authenticator)
            {
                $GoogleAuthenticator = new GoogleAuthenticator();
                $model->google_authenticator_secret = $GoogleAuthenticator->createSecret();
            }

            if ($model && $model->save())
            {
                Yii::$app->authManager->assign(Yii::$app->authManager->getRole('user'), $model->id);

                $roles = Yii::$app->request->post('roles');
                if ($roles && Yii::$app->user->identity->hasPermission(['admin', 'moderator']))
                {
                    foreach ($roles as $k => $role)
                    {
                        $role = Yii::$app->authManager->getRole($role);
                        if ($role && !empty($role)) Yii::$app->authManager->assign($role, $model->id);
                    }
                }

                // TODO: Отправлять письмо при регистрации $model->sandmail

                Yii::$app->getSession()->setFlash('success', Yii::t('app', "Пользователь успешно добавлен"));
                return $this->redirect(['update', 'id' => $model->id]);
            }
        }

        return $this->render('crud/create', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()))
        {
            $image = UploadedFile::getInstance($model, 'image');
            if (!is_null($image))
            {
                $image = new FileUpload($image, $model, 'image', '/files/uploads/users/photos/');
                $model = $image->saveAs();
            }
            else $model->image = $model->getOldAttribute('image');

            if ($model->authenticator)
            {
                $GoogleAuthenticator = new GoogleAuthenticator();
                $model->google_authenticator_secret = $GoogleAuthenticator->createSecret();
            }

            if ($model && $model->save())
            {
                $roles = Yii::$app->request->post('roles');
                if ($roles && Yii::$app->user->identity->hasPermission(['admin', 'moderator']))
                {
                    $model->unlinkAll('roles', true);
                    Yii::$app->authManager->assign(Yii::$app->authManager->getRole('user'), $model->id);

                    foreach ($roles as $k => $role)
                    {
                        $role = Yii::$app->authManager->getRole($role);
                        if ($role && !empty($role)) Yii::$app->authManager->assign($role, $model->id);
                    }
                }

                Yii::$app->getSession()->setFlash('success', Yii::t('app', "Изменения сохранены"));
                return $this->redirect(['update', 'id' => $model->id]);
            }
        }

        return $this->render('crud/update', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id) : Response
    {
        $model = $this->findModel($id);
        if ($model->delete()) Yii::$app->getSession()->setFlash('success', Yii::t('app', "Пользователь успешно удален"));
        else Yii::$app->getSession()->setFlash('warning', Yii::t('app', "Вы не можете удалить данного пользователя"));

        return $this->redirect(['index']);
    }

    /**
     * Finds the Lang model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) return $model;
        else throw new NotFoundHttpException(Yii::t('app', 'Пользователь не найден'));
    }
}