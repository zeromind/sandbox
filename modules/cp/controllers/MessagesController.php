<?php namespace app\modules\cp\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

use app\models\Languages;
use app\components\Controller;
use app\modules\cp\models\Message;
use app\modules\cp\models\SourceMessage;
use app\modules\cp\models\SourceMessageSearch;

class MessagesController extends Controller
{
    public $layout = "./sidebar";

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update'],
                        'allow' => true,
                        'roles' => ['admin', 'moderator'],
                    ]
                ]
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'index' => ['GET']
                ]
            ]
        ];
    }

    public function sidebarSettings()
    {
        return ['visibility' => false];
    }

    public function actionIndex()
    {
        $searchModel = new SourceMessageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $model = new SourceMessage();

        $data = Yii::$app->request->post();
        if ($model->load($data) && $model->save())
        {
            Yii::$app->cache->flush();

            foreach ($data['message'] as $code => $message)
            {
                $translation = Message::find()->where('language = :language AND id = :id', [':language' => $code, ':id' => $model->id])->one();
                if ($translation) $translation->translation = $message;
                else
                {
                    $translation = new Message();
                    $translation->id = $model->id;
                    $translation->language = $code;
                    $translation->translation = $message;
                }

                $translation->save();
            }

            return $this->redirect(['index']);
        }

        return $this->render('crud/create', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $translations = [];
        foreach (Languages::getBehaviorsList() as $title => $code)
        {
            $messageModel = Message::find()->where('language = :language AND id = :id', [':language' => $code, ':id' => $id])->one();
            $translations[$code] = $messageModel ? $messageModel->translation : null;
        }

        $data = Yii::$app->request->post();
        if ($model->load($data) && $model->save())
        {
            Yii::$app->cache->flush();
            Yii::$app->getSession()->setFlash('success', 'Изменения сохранены');
            foreach ($data['message'] as $code => $message)
            {
                $translation = Message::find()->where('language = :language AND id = :id', [':language' => $code, ':id' => $model->id])->one();
                if ($translation) $translation->translation = $message;
                else
                {
                    $translation = new Message();
                    $translation->id = $model->id;
                    $translation->language = $code;
                    $translation->translation = $message;
                }

                $translation->save();
            }
            return $this->redirect(['update', 'id' => $model->id]);
        }

        return $this->render('crud/update', [
            'model' => $model,
            'translations' => $translations,
        ]);
    }

    /**
     * Finds the Lang model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     * @return SourceMessage the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SourceMessage::findOne($id)) !== null) return $model;
        else throw new NotFoundHttpException(Yii::t('app', 'Перевод не найден'));
    }
}