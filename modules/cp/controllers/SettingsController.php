<?php namespace app\modules\cp\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

use app\components\Controller;
use app\modules\cp\components\FileUpload;
use app\modules\cp\models\Settings;

class SettingsController extends Controller
{
	public $layout = "./sidebar";
	
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['index', 'messages', 'languages', 'cache-flush'],
                        'allow' => true,
                        'roles' => ['admin', 'moderator']
                    ]
                ]
            ]
        ];
    }

    public function sidebarSettings()
    {
        return [
            'visibility' => true,
            'position' => 100,
            'actions' => [
                'index' => Yii::t('app', "Основные"),
                'messages' => [
                    'title' => Yii::t('app', "Переводы"),
                    'icon'  => 'icon ion-flag'
                ],
                'cache-flush' => [
                    'title' => Yii::t('app', "Сбросить кэш"),
                    'icon'  => 'icon ion-trash-b'
                ]
            ],
            'title' => Yii::t('app', "Настройки"),
            'icon' => 'icon ion-gear-b'
        ];
    }

    public function actionIndex()
    {
        $model = $this->findModel(1);
        if ($model === null) throw new NotFoundHttpException(Yii::t('app', "Страница не найдена"));

        if ($model->load(Yii::$app->request->post()))
        {
            $logotype = UploadedFile::getInstance($model, 'logotype');
            if (!is_null($logotype))
            {
                $logotype = new FileUpload($logotype, $model, 'logotype', '/files/uploads/settings/');
                $model = $logotype->saveAs();
            }
            else $model->logotype = $model->getOldAttribute('logotype');

            if ($model->save())
            {
                Yii::$app->cache->delete('app/settings');
                Yii::$app->getSession()->setFlash('success', Yii::t('app', "Изменения сохранены"));
                return $this->refresh();
            }
        }

        return $this->render('index', [
            'model' => $model
        ]);
    }

    public function actionCacheFlush()
    {
        Yii::$app->cache->delete('app/settings');
        Yii::$app->cache->flush();

        Yii::$app->getSession()->setFlash('success', Yii::t('app', "Кеш успешно сброшен"));

        return $this->goBack(Yii::$app->request->referrer);
    }

    public function actionMessages()
    {
        return $this->redirect(['/cp/messages/index']);
    }

    protected function findModel($id, $ml = true)
    {
        if ($ml) $model = Settings::find()->where('id = :id', [':id' => $id])->one();
        else $model = Settings::findOne($id);

        if ($model !== null) return $model;
        else throw new NotFoundHttpException(Yii::t('app', "Страница не найдена"));
    }
}
