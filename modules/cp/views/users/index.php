<?php /* @var $this yii\web\View */

/**
 * @var \yii\data\ActiveDataProvider $dataProvider
 * @var \app\modules\cp\models\UsersSearch $searchModel
 */

use yii\widgets\Breadcrumbs;
use yii\helpers\Html;
use yii\widgets\Pjax;

use kartik\grid\GridView;

$this->title = Yii::t('app', "Пользователи");
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', "Панель управления"), 'url' => ['/cp']];
$this->params['breadcrumbs'][] = $this->title; ?>
<div class="slim-pageheader">
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        'options' => [
            'class' => 'breadcrumb slim-breadcrumb'
        ],
        'itemTemplate' => '<li class="breadcrumb-item">{link}</li>',
        'activeItemTemplate' => '<li class="breadcrumb-item active">{link}</li>'
    ]) ?>
    <h6 class="slim-pagetitle"><?= $this->title; ?></h6>
</div>
<div class="card card-table">
    <div class="card-header d-flex align-items-center justify-content-between">
        <h6 class="slim-card-title"><?= Yii::t('app', "Список пользователей"); ?></h6>
        <div class="card-option">
            <?= Html::a(Yii::t('app', "{i} Добавить", ['i' => '<i class="icon ion-plus"></i>']), ['create'], [
                'class' => 'btn btn-sm btn-outline-primary'
            ]); ?>
        </div>
    </div>
    <?php Pjax::begin(['id' => 'users']) ?>
    <?= GridView::widget([
        'dataProvider'  => $dataProvider,
        'id'            => 'grid',
        'layout'        => $this->render('_layout'),
        'filterModel'   => $searchModel,
        'tableOptions'  => [
            'class' => 'table table-hover mg-b-0 tx-13',
        ],
        'columns'       => [
            [
                'filter' => false,
                'format' => 'html',
                'value' => function ($model) {
                    return '<img src="' . $model->photoURL . '" class="wd-60" alt="' . $model->email . '" />';
                },
                'headerOptions' => ['class' => 'wd-70'],
                'contentOptions' => [
                    'class' => 'valign-middle text-center wd-60 p-1'
                ]
            ],
            [
                'attribute' => 'full_name',
                'value' => function ($model) {
                    return '<span class="tx-inverse tx-14 tx-medium d-block">' . $model->full_name . '</span><span class="tx-11 d-block">' . $model->email . '</span>';
                },
                'format' => 'html',
                'contentOptions' => [
                    'class' => 'valign-middle'
                ],
                'headerOptions' => ['class' => 'wd-300'],
                'filterInputOptions' => [
                    'placeholder' => Yii::t('app', "Эл. почта, ФИО, Псевдоним"),
                    'class' => "form-control"
                ]
            ],
            [
                'attribute' => 'phone',
                'format' => 'html',
                'value' => function ($model) {
                    return '<code class="tx-dark tx-16">' . $model->phone . '</code>';
                },
                'contentOptions' => [
                    'class' => 'valign-middle text-center'
                ],
                'headerOptions' => ['class' => 'wd-200'],
                'filterInputOptions' => [
                    'placeholder' => $searchModel->getAttributeLabel('phone'),
                    'class' => "form-control"
                ]
            ],
            [
                'attribute' => 'status',
                'value' => function ($model) {
                    $class = 'font-weight-bold text-uppercase tx-10';
                    switch ($model->status)
                    {
                        case -1: $class .= ' text-danger'; break;
                        case 0: $class .= ' text-warning'; break;
                        case 1: $class .= ' text-success'; break;
                    }
                    return '<span class="' . $class . '">' . $model->statuses[$model->status] . '</span>';
                },
                'headerOptions' => ['class' => 'wd-200'],
                'contentOptions' => ['class' => 'valign-middle text-center'],
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => $searchModel->statuses,
                'filterWidgetOptions' => [
                    'theme' => \kartik\select2\Select2::THEME_DEFAULT
                ],
                'filterInputOptions' => [
                    'placeholder' => Yii::t('app', "Выберите статус")
                ],
                'format' => 'html'
            ],
            [
                'attribute' => 'role',
                'label' => Yii::t('app', "Роли"),
                'value' => 'permissions',
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => $searchModel->rolesList,
                'filterWidgetOptions' => [
                    'theme' => \kartik\select2\Select2::THEME_DEFAULT
                ],
                'filterInputOptions' => [
                    'placeholder' => Yii::t('app', "Выберите роль")
                ],
                'contentOptions' => ['class' => 'valign-middle'],
                'format' => 'html'
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'headerOptions' => ['class' => 'wd-150 pd-y-10'],
                'contentOptions' => [
                    'class' => 'text-center pd-y-10'
                ],
                'template' => '{update} {delete}',
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::a('<button type="button" class="btn btn-secondary btn-icon"><div class="icon ion-edit"></div></button>', $url);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<button type="button" class="btn btn-danger btn-icon"><div class="icon ion-trash-a"></div></button>', $url, [
                            'data-toggle' => 'confirm',
                            'data-confirm-text' => Yii::t('app', "Вы подтверждаете удаление?"),
                        ]);
                    }
                ]
            ]
        ],
        'pager' => [
            'nextPageCssClass' => 'page-item',
            'nextPageLabel' => '<i class="fa fa-angle-right"></i>',

            'prevPageCssClass' => 'page-item',
            'prevPageLabel' => '<i class="fa fa-angle-left"></i>',

            'disabledListItemSubTagOptions' => [
                'class' => 'page-link'
            ],

            'options' => [
                'class' => 'pagination bd-t bg-gray-200 justify-content-center text-center mg-b-0 pd-y-15'
            ],
            'pageCssClass' => 'page-item',
            'linkOptions' => [
                'class' => 'page-link'
            ]
        ]
    ]); ?>
    <?php Pjax::end() ?>
</div>

