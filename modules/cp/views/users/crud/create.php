<?php /** @var \app\modules\cp\models\User $model */
use yii\widgets\Breadcrumbs;

$this->title = Yii::t('app', "Добавить пользователя");
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', "Пользователи"), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title; ?>
<div class="slim-pageheader">
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        'options' => [
            'class' => 'breadcrumb slim-breadcrumb'
        ],
        'itemTemplate' => '<li class="breadcrumb-item">{link}</li>',
        'activeItemTemplate' => '<li class="breadcrumb-item active">{link}</li>'
    ]) ?>
    <h6 class="slim-pagetitle"><?= $this->title; ?></h6>
</div>
<div class="section-wrapper">
    <?= $this->render('_template', [
        'model' => $model
    ]); ?>
</div>