<?php
/**
 * @var $name
 * @var $value
 * @var $label
 * @var $checked
 * @var $default
 */
?>
<label class="ckbox">
    <input name="<?= $name; ?>" type="checkbox" value="<?= $value; ?>"<?= $default ? $default : ($checked ? $checked : ''); ?> />
    <span><?= $label; ?></span>
</label>