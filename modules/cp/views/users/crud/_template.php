<?php /** @var $model \app\modules\cp\models\User */
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap4\ActiveForm;

use kartik\select2\Select2;
use borales\extensions\phoneInput\PhoneInput;

use app\modules\cp\models\AuthItem;

$form = ActiveForm::begin([
    'options' => ['enctype' => 'multipart/form-data'],
    'errorCssClass' => 'is-invalid',
    'fieldClass' => \yii\bootstrap4\ActiveField::class
]); ?>
<?= $form->errorSummary($model, ['class' => 'bd-l bd-3 bd-danger bg-gray-100 pd-x-20 pd-y-15 mg-b-20']); ?>
<div class="row mb-5">
    <div class="col-12 col-md-4 col-lg-3">
        <label class="section-title"><?= Yii::t('app', "Фотография"); ?></label>
        <div class="row row-xs">
            <div class="col-12">
                <div class="preview mg-b-20 text-center">
                    <span class="d-inline-block bd pd-5">
                        <img src="<?= $model->photoURL; ?>" alt="image" class="mx-wd-100p" />
                    </span>
                </div>
                <?= $form->field($model, 'image', [
                    'template'  => '{input}{label}',
                    'options'   => ['class' => 'custom-file'],
                    'inputOptions' => ['class' => 'custom-file-input', 'type'  => 'file', 'accept' => ".jpg,.png"],
                    'labelOptions' => ['class' => 'custom-file-label']
                ])->label(Yii::t('app', "Выберите файл")); ?>
            </div>
        </div>
    </div>
    <div class="col-12 col-md-4 col-lg-5">
        <label class="section-title"><?= Yii::t('app', "Информация"); ?></label>
        <div class="row row-xs">
            <div class="col-12 col-md-6 col-lg-4"><?= $form->field($model, 'last_name')->textInput(); ?></div>
            <div class="col-12 col-md-6 col-lg-4"><?= $form->field($model, 'first_name')->textInput(); ?></div>
            <div class="col-12 col-md-6 col-lg-4"><?= $form->field($model, 'second_name')->textInput(); ?></div>
            <div class="col-12 col-md-6"><?= $form->field($model, 'nickname')->textInput(); ?></div>
            <div class="col-12 col-md-6">
                <?= $form->field($model, 'gender')->widget(Select2::class, [
                    'theme' => Select2::THEME_DEFAULT,
                    'data' => $model->genders,
                    'hideSearch' => true
                ]); ?>
            </div>
            <div class="col-12 col-md-6">
                <?= $form->field($model, 'email')->textInput(); ?>
                <?php if ($model->isNewRecord) echo $form->field($model, 'sendmail', [
                    'template' => '{beginLabel}{input}<span>{labelTitle}</span>{endLabel}{error}{hint}',
                    'options' => [
                        'class' => 'ckbox'
                    ],
                    'inputOptions' => [
                        'type' => 'checkbox',
                        'class' => false,
                        'checked' => true,
                        'value' => 'yes'
                    ]
                ]); ?>
            </div>
            <div class="col-12 col-md-6">
                <?= $form->field($model, 'phone', [
                    'template'  => '{label}<br />{input}'
                ])->widget(PhoneInput::class, [
                    'options' => [
                        'customContainer' => 'd-block'
                    ],
                    'jsOptions' => [
                        'customContainer' => 'd-block',
                        'preferredCountries' => ['ua', 'ru'],
                    ]
                ]); ?>
            </div>
        </div>
    </div>
    <div class="col-12 col-md-4 col-lg-4">
        <label class="section-title"><?= Yii::t('app', "Системные настройки"); ?></label>
        <div class="row row-xs">
            <div class="col-12">
                <?= Html::checkboxList('roles',
                    array_keys(ArrayHelper::map(Yii::$app->authManager->getRolesByUser($model->id), 'name', 'description')),
                    ArrayHelper::map(AuthItem::find()->orderBy('description ASC')->all(), 'name', 'description'),
                    [
                        'item' => function ($index, $label, $name, $checked, $value) {
                            return $this->render('_checkbox', [
                                'name' => $name,
                                'value' => $value,
                                'label' => $label,
                                'checked' => $checked ? ' checked="checked"' : false,
                                'default' => ($value == 'user') ? ' checked="checked" disabled' : false
                            ]);
                        },
                        'class' => 'mg-b-20'
                    ]
                ); ?>
            </div>
            <div class="col-12 mg-b-20">
                <?php if ($model->isNewRecord || empty($model->google_authenticator_secret)) : ?>
                    <label class="section-title"><?= $model->getAttributeLabel('google_authenticator_secret'); ?></label>
                <?php echo $form->field($model, 'authenticator', [
                    'template' => '{beginLabel}{input}<span>{labelTitle}</span>{endLabel}{error}{hint}',
                    'options' => [
                        'class' => 'ckbox'
                    ],
                    'inputOptions' => [
                        'type' => 'checkbox',
                        'class' => false,
                        'checked' => $model->isNewRecord ? true : false,
                        'value' => 'yes'
                    ]
                ])->label(Yii::t('app', "Активировать")); ?>
                <?php else : ?>
                    <label class="section-title"><?= $model->getAttributeLabel('google_authenticator_secret'); ?></label>
                <p class="text-center">
                    <code class="tx-32 tx-black blur hover-no-blur no-select"><?= wordwrap($model->google_authenticator_secret, 4, ' ', true); ?></code>
                </p>
                <?php endif; ?>
            </div>
            <div class="col-12 col-md-6">
                <?= $form->field($model, 'type')->widget(Select2::class, [
                    'theme' => Select2::THEME_DEFAULT,
                    'data' => $model->types,
                    'hideSearch' => true
                ]); ?>
            </div>
            <div class="col-12 col-md-6">
                <?= $form->field($model, 'status')->widget(Select2::class, [
                    'theme' => Select2::THEME_DEFAULT,
                    'data' => $model->statuses,
                    'hideSearch' => true
                ]); ?>
            </div>
        </div>
    </div>
</div>

<div class="col-12 text-right">
    <button type="submit" class="btn btn-xl btn-outline-primary">
        <span class="fs-12 text-uppercase"><?= Yii::t('app', $model->isNewRecord ? "Добавить" : "Сохранить"); ?></span>
    </button>
</div>
<?php ActiveForm::end(); ?>
