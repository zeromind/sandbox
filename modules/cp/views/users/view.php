<?php /** @var \app\modules\cp\models\User $model */
use yii\widgets\Breadcrumbs;

$this->title = Yii::t('app', "Просмотр &raquo; {s}", ['s' => $model->full_name]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', "Пользователи"), 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->full_name; ?>
<div class="slim-pageheader">
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        'options' => [
            'class' => 'breadcrumb slim-breadcrumb'
        ],
        'itemTemplate' => '<li class="breadcrumb-item">{link}</li>',
        'activeItemTemplate' => '<li class="breadcrumb-item active">{link}</li>'
    ]) ?>
    <h6 class="slim-pagetitle"><?= $this->title; ?></h6>
</div>
<div class="row row-sm">
    <div class="col-12 col-md-6 col-lg-5">
        <div class="card-contact">
            <div class="tx-center">
                <a href="<?= \yii\helpers\Url::to(['/cp/users/update', 'id' => $model->id]); ?>">
                    <img src="<?= $model->photoURL; ?>" class="card-img" alt="<?= $model->full_name; ?>">
                </a>
                <h5 class="mg-t-10 mg-b-5">
                    <a href="<?= \yii\helpers\Url::to(['/cp/users/update', 'id' => $model->id]); ?>" class="contact-name"><?= $model->full_name; ?></a>
                </h5>
                <p>@<?= $model->nickname; ?></p>
            </div>
            <p class="contact-item">
                <span>Phone:</span>
                <span>+1 012 3456 789</span>
            </p>
            <p class="contact-item">
                <span>Email:</span>
                <a href="mailto:<?= $model->email; ?>"><?= $model->email; ?></a>
            </p>
        </div>
    </div>
</div>