<?php
/** @var $item array */
/** @var $controller_id string */
/** @var $action_id string */
?>
<?php if ($item['settings']['actions'] && count($item['settings']['actions']) > 0) : ?>
    <li class="sidebar-nav-item with-sub">
        <a href="javascript:void(0)" aria-expanded="true" <?= ($controller_id == $item['url'] ) ? ' class="sidebar-nav-link active"' : ' class="sidebar-nav-link"' ?>>
            <i class="<?= $item['settings']['icon']; ?>"></i>
            <span><?= $item['settings']['title']; ?></span>
        </a>
        <ul class="nav sidebar-nav-sub">
            <?php foreach ($item['settings']['actions'] as $action => $action_item) : ?>
            <?php if (is_array($action_item)) : ?>
                    <li class="nav-sub-item">
                        <a href="<?= \yii\helpers\Url::toRoute(['/cp/' . $item['url'] . '/' . $action]); ?>"<?= ($controller_id == $item['url'] && $action_id == $action) ? ' class="nav-sub-link active"' : ' class="nav-sub-link"'; ?>>
                            <i class="<?= $action_item['icon']; ?> wd-20 d-inline-block pos-relative l--30 tx-18 text-center"></i>
                            <span class="pos-relative l--20"><?= $action_item['title']; ?></span>
                        </a>
                    </li>
            <?php else : ?>
                    <li class="nav-sub-item">
                        <a href="<?= \yii\helpers\Url::toRoute(['/cp/' . $item['url'] . '/' . $action]); ?>"<?= ($controller_id == $item['url'] && $action_id == $action) ? ' class="nav-sub-link active"' : ' class="nav-sub-link"'; ?>>
                            <span><?= $action_item; ?></span>
                        </a>
                    </li>
            <?php endif; ?>
            <?php endforeach; ?>
        </ul>
    </li>
<?php else : ?>
    <li class="sidebar-nav-item">
        <a href="<?= \yii\helpers\Url::toRoute(['/cp/' . $item['url']]); ?>"<?= ($controller_id == $item['url'] && ($action_id == 'index' || $action_id == 'update' || $action_id == 'view' || $action_id == 'create')) ? ' class="sidebar-nav-link active"' : ' class="sidebar-nav-link"' ?>>
            <i class="<?= $item['settings']['icon']; ?>"></i>
            <span><?= $item['settings']['title']; ?></span>
        </a>
    </li>
<?php endif; ?>