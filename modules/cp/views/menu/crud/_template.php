<?php
/** @var Menu $model */

use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\ActiveField;

use app\models\Languages;
use app\models\Menu;
use yii\helpers\ArrayHelper;

$form = ActiveForm::begin([
    'options' => ['enctype' => 'multipart/form-data'],
    'errorCssClass' => 'is-invalid',
    'fieldClass' => ActiveField::class
]); ?>
<?= $form->errorSummary($model, ['class' => 'bd-l bd-3 bd-danger bg-gray-100 pd-x-20 pd-y-15 mg-b-20']); ?>
<div class="row mb-5">
    <div class="col-12 col-md-4 col-lg-5">
        <label class="section-title"><?= Yii::t('app', "Данные"); ?></label>
        <div class="row row-xs">
            <div class="col-12 col-md-6"><?= $form->field($model, 'name')->textInput(); ?></div>
            <div class="col-12 col-md-6"><?= $form->field($model, 'url')->textInput(); ?></div>
            <div class="col-12 col-md-6 col-lg-4"><?= $form->field($model, 'group')->dropDownList(Menu::getMenuGroups()); ?></div>
            <div class="col-12 col-md-6 col-lg-4"><?= $form->field($model, 'sort_order')->textInput(['type' => 'number', 'value' => $model->sort_order ?: 0]); ?></div>
            <div class="col-12 col-lg-4">
                <?= $form->field($model, 'parent_id')->dropDownList(ArrayHelper::merge(
                    [Yii::t('app', "Нет")],
                    ArrayHelper::map($model->parents, 'id', 'name')
                )); ?>
            </div>
        </div>
    </div>
    <div class="col-12 col-md-4 col-lg-5">
        <label class="section-title"><?= Yii::t('app', "Переводы"); ?></label>
        <div class="card">
            <div class="card-header">
                <ul class="nav nav-tabs card-header-tabs">
                    <?php $first = true; foreach (Languages::getBehaviorsList() as $language => $lang) : ?>
                        <li class="nav-item">
                            <a class="nav-link<?= $first ? ' active show' : ''; ?>" href="#language-<?= $lang; ?>" data-toggle="tab">
                                <?= \yii\helpers\Html::img('/images/flags/flag_' . $lang . '.svg', ['width' => '16']); ?>
                                <?= $language; ?>
                            </a>
                        </li>
                    <?php $first = false; endforeach; ?>
                </ul>
            </div>
            <div class="card-body">
                <div class="tab-content">
                    <?php $first = true; foreach (Languages::getBehaviorsList() as $language => $lang) : ?>
                        <div class="tab-pane<?= $first ? ' active show' : ''; ?>" id="language-<?= $lang; ?>">
                            <?= $form->field($model, 'name_' . $lang)->textInput()->label($model->getAttributeLabel('name')); ?>
                        </div>
                    <?php $first = false; endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-12 text-right">
    <button type="submit" class="btn btn-xl btn-outline-primary">
        <span class="fs-12 text-uppercase"><?= Yii::t('app', $model->isNewRecord ? "Добавить" : "Сохранить"); ?></span>
    </button>
</div>
<?php ActiveForm::end(); ?>
