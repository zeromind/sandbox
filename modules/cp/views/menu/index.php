<?php /* @var $this yii\web\View */

/**
 * @var \yii\data\ActiveDataProvider $dataProvider
 * @var \app\modules\cp\models\MenuSearch $searchModel
 */

use kartik\select2\Select2;
use yii\widgets\Breadcrumbs;
use yii\widgets\Pjax;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use app\models\Menu;

$this->title = Yii::t('app', "Меню");
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', "Панель управления"), 'url' => ['/cp']];
$this->params['breadcrumbs'][] = $this->title; ?>
<div class="slim-pageheader">
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        'options' => [
            'class' => 'breadcrumb slim-breadcrumb'
        ],
        'itemTemplate' => '<li class="breadcrumb-item">{link}</li>',
        'activeItemTemplate' => '<li class="breadcrumb-item active">{link}</li>'
    ]) ?>
    <h6 class="slim-pagetitle"><?= $this->title; ?></h6>
</div>
<div class="card card-table">
    <div class="card-header d-flex align-items-center justify-content-between">
        <h6 class="slim-card-title"><?= Yii::t('app', "Пункты меню"); ?></h6>
        <div class="card-option">
            <?= Html::a(Yii::t('app', "{i} Добавить", ['i' => '<i class="icon ion-plus"></i>']), ['create'], [
                'class' => 'btn btn-sm btn-outline-primary'
            ]); ?>
        </div>
    </div>
    <?php Pjax::begin(['id' => 'menu']) ?>
    <?= GridView::widget([
        'dataProvider'  => $dataProvider,
        'id'            => 'grid',
        'layout'        => $this->render('_layout'),
        'filterModel'   => $searchModel,
        'tableOptions'  => [
            'class' => 'table table-hover mg-b-0 tx-13',
        ],
        'columns'       => [
            [
                'class' => 'yii\grid\SerialColumn',
                'contentOptions' => ['class' => 'valign-middle text-center'],
            ],
            [
                'attribute' => 'name',
                'contentOptions' => ['class' => 'valign-middle'],
            ],
            [
                'attribute' => 'url',
                'contentOptions' => ['class' => 'valign-middle'],
                'content' => function ($model) {
                    return Html::a($model->url, $model->url);
                }
            ],
            [
                'attribute' => 'parent_id',
                'content' => function ($data) {
                    $arr = ArrayHelper::merge([Yii::t('app', "Не выбран")], ArrayHelper::map(Menu::findAll(['id' => $data->parent_id]), 'id', 'name'));
                    if ($data->parent_id && empty($arr[$data->parent_id]) == false) {
                        return $arr[$data->parent_id];
                    } else {
                        return Yii::t('app', "Не выбран");
                    }
                },
                'headerOptions' => ['class' => 'wd-200'],
                'contentOptions' => ['class' => 'valign-middle text-center'],
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => [Yii::t('app', "Не выбран")] + ArrayHelper::map($searchModel->getParents(true), 'id', 'name'),
                'filterWidgetOptions' => [
                    'theme' => Select2::THEME_DEFAULT
                ],
                'filterInputOptions' => [
                    'placeholder' => Yii::t('app', "Выберите родителя")
                ],
                'format' => 'html'
            ],
            [
                'attribute' => 'group',
                'value' => function ($model) {
                    return $model->menuGroups[$model->group];
                },
                'headerOptions' => ['class' => 'wd-200'],
                'contentOptions' => ['class' => 'valign-middle text-center'],
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => $searchModel->menuGroups,
                'filterWidgetOptions' => [
                    'theme' => Select2::THEME_DEFAULT
                ],
                'filterInputOptions' => [
                    'placeholder' => Yii::t('app', "Выберите группу")
                ],
                'format' => 'html'
            ],
            [
                'attribute' => 'sort_order',
                'headerOptions' => ['style' => 'width: 50px;'],
                'contentOptions' => ['class' => 'valign-middle text-center'],
                'filterInputOptions' => [
                    'class'       => 'form-control'
                ],
                'value' => function ($model) {
                    return '<span class="badge badge-primary tx-12 wd-30 pd-y-8">' . $model->sort_order . '</span>';
                },
                'format' => 'html'
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'headerOptions' => ['class' => 'wd-150 pd-y-10'],
                'contentOptions' => [
                    'class' => 'text-center pd-y-10'
                ],
                'template' => '{update} {delete}',
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::a('<button type="button" class="btn btn-secondary btn-icon"><div class="icon ion-edit"></div></button>', $url);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<button type="button" class="btn btn-danger btn-icon"><div class="icon ion-trash-a"></div></button>', $url, [
                            'data-toggle' => 'confirm',
                            'data-confirm-text' => Yii::t('app', "Вы подтверждаете удаление?"),
                        ]);
                    }
                ]
            ]
        ],
        'pager' => [
            'nextPageCssClass' => 'page-item',
            'nextPageLabel' => '<i class="fa fa-angle-right"></i>',

            'prevPageCssClass' => 'page-item',
            'prevPageLabel' => '<i class="fa fa-angle-left"></i>',

            'disabledListItemSubTagOptions' => [
                'class' => 'page-link'
            ],

            'options' => [
                'class' => 'pagination bd-t bg-gray-200 justify-content-center text-center mg-b-0 pd-y-15'
            ],
            'pageCssClass' => 'page-item',
            'linkOptions' => [
                'class' => 'page-link'
            ]
        ]
    ]); ?>
    <?php Pjax::end() ?>
</div>