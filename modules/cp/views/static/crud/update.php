<?php /** @var \app\modules\cp\models\StaticPage $model */
use yii\widgets\Breadcrumbs;

$this->title = Yii::t('app', "Редактирование » {s}", ['s' => $model->title]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', "Статические страницы"), 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->title; ?>
<div class="slim-pageheader">
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        'options' => [
            'class' => 'breadcrumb slim-breadcrumb'
        ],
        'itemTemplate' => '<li class="breadcrumb-item">{link}</li>',
        'activeItemTemplate' => '<li class="breadcrumb-item active">{link}</li>'
    ]) ?>
    <h6 class="slim-pagetitle"><?= $this->title; ?></h6>
</div>
<div class="section-wrapper">
    <?= $this->render('_template', [
        'model' => $model
    ]); ?>
</div>