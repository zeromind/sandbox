<?php

/** @var $model \app\modules\cp\models\StaticPage */

use dosamigos\tinymce\TinyMce;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;

$languages = \app\models\Languages::getBehaviorsList();

$form = ActiveForm::begin([
    'options' => ['enctype' => 'multipart/form-data'],
    'errorCssClass' => 'is-invalid',
    'fieldClass' => \yii\bootstrap4\ActiveField::class
]); ?>
<?= $form->errorSummary($model, ['class' => 'bd-l bd-3 bd-danger bg-gray-100 pd-x-20 pd-y-15 mg-b-20']); ?>
<div class="row row-xs mb-5">
    <div class="col-12 col-xl-8">
        <div class="card">
            <div class="card-header">
                <ul class="nav nav-tabs card-header-tabs">
                    <li class="nav-item">
                        <a class="nav-link active" href="#language-original" data-toggle="tab">Оригинал</a>
                    </li>
                    <?php $first = true; foreach ($languages as $title => $code) : ?>
                        <li class="nav-item">
                            <a class="nav-link" href="#language-<?= $code; ?>" data-toggle="tab"><?= $title; ?></a>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
            <div class="card-body">
                <div class="tab-content">
                    <div class="tab-pane active" id="language-original">
                        <div class="row row-xs">
                            <div class="col-12"><?= $form->field($model, 'title')->textInput(); ?></div>
                            <div class="col-12">
                                <?= $form->field($model, 'content')->widget(TinyMce::class, [
                                    'language' => substr(Yii::$app->language, 0, 2),
                                    'clientOptions' => [
                                        'branding' => false
                                    ]
                                ]); ?>
                            </div>
                        </div>
                    </div>
                    <?php $first = true; foreach ($languages as $title => $code) : ?>
                        <div class="tab-pane" id="language-<?= $code; ?>">
                            <div class="row row-xs">
                                <div class="col-12"><?= $form->field($model, 'title_' . $code)->textInput(); ?></div>
                                <div class="col-12">
                                    <?= $form->field($model, 'content_' . $code)->widget(TinyMce::class, [
                                        'language' => substr(Yii::$app->language, 0, 2),
                                        'clientOptions' => [
                                            'branding' => false
                                        ]
                                    ]); ?>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12 col-xl-4">
        <div class="section-wrapper">
            <label class="section-title"><?= Yii::t('app', "Настройки"); ?></label>
            <div class="row row-xs">
                <div class="col-12"><?= $form->field($model, 'url')->textInput(); ?></div>
                <div class="col-12"><?= $form->field($model, 'public')->dropDownList([0 => 'Нет', 1 => 'Да']); ?></div>
                <?php if ($model->isNewRecord == false) : ?>
                <div class="col-12">
                    <?= Html::label( $model->getAttributeLabel('views')); ?>:
                    <?= $model->views; ?>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <div class="col-12 col-xl-8 mg-t-30">
        <div class="text-right">
            <button type="submit" class="btn btn-xl btn-outline-primary">
                <span class="fs-12 text-uppercase"><?= Yii::t('app', $model->isNewRecord ? "Добавить" : "Сохранить"); ?></span>
            </button>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>
