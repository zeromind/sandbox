<?php

/**
 * @var $this yii\web\View
 * @var \yii\data\ActiveDataProvider $dataProvider
 * @var \app\modules\cp\models\StaticPageSearch $searchModel
 */

use kartik\select2\Select2;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use yii\helpers\Html;
use yii\widgets\Pjax;

use kartik\grid\GridView;

$this->title = Yii::t('app', "Статические страницы");
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', "Панель управления"), 'url' => ['/cp']];
$this->params['breadcrumbs'][] = $this->title; ?>
<div class="slim-pageheader">
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        'options' => [
            'class' => 'breadcrumb slim-breadcrumb'
        ],
        'itemTemplate' => '<li class="breadcrumb-item">{link}</li>',
        'activeItemTemplate' => '<li class="breadcrumb-item active">{link}</li>'
    ]) ?>
    <h6 class="slim-pagetitle"><?= $this->title; ?></h6>
</div>
<div class="card card-table">
    <div class="card-header d-flex align-items-center justify-content-between">
        <h6 class="slim-card-title"><?= Yii::t('app', "Список страниц"); ?></h6>
        <div class="card-option">
            <?= Html::a(Yii::t('app', "{i} Добавить", ['i' => '<i class="icon ion-plus"></i>']), ['create'], [
                'class' => 'btn btn-sm btn-outline-primary'
            ]); ?>
        </div>
    </div>
    <?php Pjax::begin(['id' => 'static-pages']) ?>
    <?= GridView::widget([
        'dataProvider'  => $dataProvider,
        'id'            => 'grid',
        'layout'        => $this->render('_layout'),
        'filterModel'   => $searchModel,
        'tableOptions'  => [
            'class' => 'table table-hover mg-b-0 tx-13',
        ],
        'columns'       => [
            [
                'attribute' => 'title',
                'contentOptions' => [
                    'class' => 'valign-middle'
                ],
                'filterInputOptions' => [
                    'placeholder' => Yii::t('app', "Заголовок"),
                    'class' => "form-control"
                ]
            ],
            [
                'attribute' => 'url',
                'value' => function ($model) {
                    return Html::a($model->url, Url::to('/' . $model->url));
                },
                'contentOptions' => [
                    'class' => 'valign-middle'
                ],
                'filterInputOptions' => [
                    'placeholder' => Yii::t('app', "Ссылка"),
                    'class' => "form-control"
                ],
                'format' => 'html'
            ],
            [
                'attribute' => 'views',
                'contentOptions' => [
                    'class' => 'valign-middle text-center'
                ],
                'headerOptions' => ['class' => 'wd-150']
            ],
            [
                'attribute' => 'public',
                'filterType' => GridView::FILTER_SELECT2,
                'filterWidgetOptions' => [
                    'theme' => Select2::THEME_DEFAULT,
                    'hideSearch' => true
                ],
                'filter' => [
                    0 => Yii::t('app', "Нет"),
                    1 => Yii::t('app', "Да")
                ],
                'contentOptions' => [
                    'class' => 'valign-middle'
                ],
                'headerOptions' => ['class' => 'wd-100'],
                'format' => 'html'
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'headerOptions' => ['class' => 'wd-150 pd-y-10'],
                'contentOptions' => [
                    'class' => 'text-center pd-y-10'
                ],
                'template' => '{update} {delete}',
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::a('<button type="button" class="btn btn-secondary btn-icon"><div class="icon ion-edit"></div></button>', $url);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<button type="button" class="btn btn-danger btn-icon"><div class="icon ion-trash-a"></div></button>', $url, [
                            'data-toggle' => 'confirm',
                            'data-confirm-text' => Yii::t('app', "Вы подтверждаете удаление?"),
                        ]);
                    }
                ]
            ]
        ],
        'pager' => [
            'nextPageCssClass' => 'page-item',
            'nextPageLabel' => '<i class="fa fa-angle-right"></i>',

            'prevPageCssClass' => 'page-item',
            'prevPageLabel' => '<i class="fa fa-angle-left"></i>',

            'disabledListItemSubTagOptions' => [
                'class' => 'page-link'
            ],

            'options' => [
                'class' => 'pagination bd-t bg-gray-200 justify-content-center text-center mg-b-0 pd-y-15'
            ],
            'pageCssClass' => 'page-item',
            'linkOptions' => [
                'class' => 'page-link'
            ]
        ]
    ]); ?>
    <?php Pjax::end() ?>
</div>

