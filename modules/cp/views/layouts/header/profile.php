<?php
/* @var $this yii\web\View */
/* @var $user app\modules\user\models\User */

use yii\helpers\Url;

use app\modules\cp\components\Activity;
use app\modules\cp\components\Notifications;

/** @var \app\models\User $user */
$user = Yii::$app->user->getIdentity();
?>
<div class="dropdown dropdown-c">
    <a href="#" class="logged-user pd-r-15" data-toggle="dropdown">
        <img src="<?= $user->photoURL; ?>" alt="<?= $user->full_name; ?>">
        <span><?= $user->first_name; ?><br /><small><?= $user->permission; ?></small></span>
        <i class="fa fa-angle-down mg-l-15"></i>
    </a>
    <div class="dropdown-menu dropdown-menu-right">
        <div class="row row-xs bd-b bd-gray-200 mb-2 pb-2">
            <div class="col">
                <div class="text-center tx-bold tx-black">
                    <span>Google Authenticator</span>
                    <p class="tx-18 m-0 tx-gray-700">
                        <span id="two-step-timer"></span>
                    </p>
                </div>
                <script type="text/javascript">
                    function startTimer(duration)
                    {
                        let timer = duration, hours, minutes, seconds;
                        setInterval(function ()
                        {
                            hours   = parseInt((timer / 3600) % 24, 10);
                            minutes = parseInt((timer / 60) % 60, 10);
                            seconds = timer - (hours * 60 * 60 + minutes * 60);

                            hours   = hours   < 10 ? "0" + hours : hours;
                            minutes = minutes < 10 ? "0" + minutes : minutes;
                            seconds = seconds < 10 ? "0" + seconds : seconds;

                            $('#two-step-timer').html(hours + ":" + minutes + ":" + seconds);

                            timer--;
                        }, 1000);
                    }

                    startTimer(<?= Yii::$app->getSession()->get('two-step-expired') - time(); ?>)
                </script>
            </div>
        </div>
        <nav class="nav">
            <?php if (!Activity::disabled()) : ?><a href="<?= Url::toRoute(['/cp/activity']); ?>" class="nav-link">
                <i class="icon ion-ios-bolt"></i>
                <span><?= Yii::t('app', "Логи"); ?></span>
            </a><?php endif; ?>
            <?php if (!Notifications::disabled()) : ?><a href="<?= Url::toRoute(['/cp/notifications']); ?>" class="nav-link">
                    <i class="icon ion-ios-bell"></i>
                <span><?= Yii::t('app', "Уведомления"); ?></span>
            </a><?php endif; ?>
            <a href="<?= Url::toRoute(['/user/default/sign-out']); ?>" class="nav-link">
                <i class="icon ion-forward"></i>
                <span><?= Yii::t('app', "Выйти"); ?></span>
            </a>
        </nav>
    </div>
</div>