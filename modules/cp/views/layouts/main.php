<?php /* @var $this yii\web\View */

use yii\helpers\Html;
use app\modules\cp\assets\AppAsset;
use app\modules\cp\components\Activity;
use app\modules\cp\components\Notifications;

AppAsset::register($this);
$this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <!-- Required meta tags -->
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Meta -->
    <?= Html::csrfMetaTags() ?>

    <title><?= Html::encode($this->title) ?></title>

    <link rel="apple-touch-icon" sizes="180x180" href="/images/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/images/favicon/favicon-16x16.png">
    <link rel="manifest" href="/images/favicon/manifest.json">
    <link rel="mask-icon" href="/images/favicon/safari-pinned-tab.svg" color="#594df7">
    <link rel="shortcut icon" href="/images/favicon/favicon.ico">

    <meta name="msapplication-TileColor" content="#594df7">
    <meta name="msapplication-config" content="/images/favicon/browserconfig.xml">
    <meta name="theme-color" content="#858585">

    <?php $this->head() ?>

</head>
<body class="header-two">
<?php $this->beginBody() ?>
<div class="slim-header with-sidebar">
    <div class="container-fluid">
        <div class="slim-header-left">
            <div class="slim-logo text-center">
                <a href="<?= \yii\helpers\Url::toRoute(['/']); ?>">
                    <?= Yii::$app->name; ?>
                </a>
            </div>
            <a href="javascript:void();" id="slimSidebarMenu" class="slim-sidebar-menu"><span></span></a>
        </div><!-- slim-header-left -->
        <div class="slim-header-right">
            <?php if (!Activity::disabled()) echo $this->render('header/activity'); ?>
            <?php if (!Notifications::disabled()) echo $this->render('header/notifications'); ?>
            <?= $this->render('header/profile'); ?>
        </div><!-- header-right -->
    </div><!-- container-fluid -->
</div><!-- slim-header -->

<div class="slim-body">
    <?= $content; ?>
</div><!-- slim-body -->

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>