<?php use app\modules\cp\components\SidebarMenu;
$this->beginContent('@app/modules/cp/views/layouts/main.php');
$sidebarMenu = new SidebarMenu(); ?>
<div class="slim-sidebar">
    <label class="sidebar-label text-center tx-gray-600"><?= Yii::t('app', "Навигация"); ?></label>

    <ul class="nav nav-sidebar">
        <?= $sidebarMenu->items(); ?>
    </ul>
</div>
<!-- slim-sidebar -->

<div class="slim-mainpanel">
    <div class="alerts--wrapper"><?= \app\components\widgets\Alert::widget(); ?></div>
    <div class="container"><?= $content; ?></div>
</div>
<?php $this->endContent(); ?>