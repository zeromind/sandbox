<?php
/**
 * @var $model \app\modules\cp\models\Settings
 * @var $form \yii\bootstrap4\ActiveForm
 * @var $lang string
 * @var $sourceLanguage string
 */

use kartik\select2\Select2;

$_lang = empty($lang) ? $sourceLanguage : $lang;
$lang = empty($lang) ? null : '_' . $lang;
?>
<div class="row row-xs">
    <div class="col-12 col-lg-3 col-xl-2">
        <label class="section-title"><?= $model->getAttributeLabel('logotype'); ?></label>
        <div class="row row-xs">
            <div class="col-12">
                <div class="preview mg-b-20 text-center">
                    <span class="d-inline-block bd pd-5">
                        <img src="<?= $model->getLogotypeURL($_lang); ?>" alt="image" class="mx-wd-100p" />
                    </span>
                </div>
                <?= $form->field($model, 'logotype' . $lang, [
                    'template'  => '{input}{label}',
                    'options'   => ['class' => 'custom-file'],
                    'inputOptions' => ['class' => 'custom-file-input', 'type'  => 'file', 'accept' => ".jpg,.png"],
                    'labelOptions' => ['class' => 'custom-file-label']
                ])->label(Yii::t('app', "Выберите файл")); ?>
            </div>
        </div>
    </div>
    <div class="col-12 col-lg-9 col-xl-10">
        <label class="section-title"><?= Yii::t('app', "Настройки сайта"); ?></label>
        <div class="row row-xs">
            <div class="col-12">
                <?= $form->field($model, 'title' . $lang, ['template' => '{label}{input}{error}{hint}'])
                    ->textInput(['class' => 'form-control fs-12'])->label($model->getAttributeLabel('title')); ?>
            </div>
            <div class="col-12">
                <?= $form->field($model, 'keywords' . $lang)->widget(Select2::class, [
                    'theme' => Select2::THEME_DEFAULT,
                    'hideSearch' => false,
                    'maintainOrder' => true,
                    'options' => ['multiple' => true],
                    'pluginOptions' => [
                        'tags' => true,
                        'tokenSeparators' => [',', ';'],
                        'maximumInputLength' => 24
                    ],
                ])->label($model->getAttributeLabel('keywords')); ?>
            </div>
            <div class="col-12">
                <?= $form->field($model, 'description', ['template' => '{label}{input}{error}{hint}'])
                    ->textarea(['rows' => 8, 'class' => 'form-control fs-12'])->label($model->getAttributeLabel('description')); ?>
            </div>
        </div>
    </div>
</div>