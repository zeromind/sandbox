<?php
/**
 * @var $model \app\modules\cp\models\Settings
 * @var $this \yii\web\View
 */

use yii\bootstrap4\ActiveForm;
use app\models\Languages;

$sourceLanguage = strtolower(substr(Yii::$app->sourceLanguage, 0, 2));

$form = ActiveForm::begin([
    'options' => ['enctype' => 'multipart/form-data'],
    'errorCssClass' => 'is-invalid',
    'fieldClass' => \yii\bootstrap4\ActiveField::class
]); ?>
<?= $form->errorSummary($model, ['class' => 'bd-l bd-3 bd-danger bg-gray-100 pd-x-20 pd-y-15 mg-b-20']); ?>

<div class="row row-xs mb-5">
    <div class="col-12">

        <div class="card bd-0 m-b-40">
            <div class="card-header bg-secondary">
                <ul class="nav nav-tabs nav-tabs-for-dark card-header-tabs">
                    <li class="nav-item">
                        <a class="nav-link active show" href="#language-<?= $sourceLanguage; ?>" data-toggle="tab">
                            <?= \yii\helpers\Html::img('/images/flags/flag_' . $sourceLanguage . '.svg', ['width' => '16']); ?>
                            <?= Yii::t('app', "Оригинал"); ?>
                        </a>
                    </li>
                    <?php foreach (Languages::getBehaviorsList() as $language => $lang) : ?>
                    <li class="nav-item">
                        <a class="nav-link" href="#language-<?= $lang; ?>" data-toggle="tab">
                            <?= \yii\helpers\Html::img('/images/flags/flag_' . $lang . '.svg', ['width' => '16']); ?>
                            <?= $language; ?>
                        </a>
                    </li>
                    <?php endforeach; ?>
                </ul>
            </div>
            <div class="card-body bd bd-t-0">
                <div class="tab-content">
                    <div class="tab-pane active show" id="language-<?= $sourceLanguage; ?>">
                        <?= $this->render('partial/_language', compact('form', 'model', 'sourceLanguage')); ?>
                    </div>
                    <?php foreach (Languages::getBehaviorsList() as $language => $lang) : ?>
                    <div class="tab-pane" id="language-<?= $lang; ?>">
                        <?= $this->render('partial/_language', compact('form', 'model', 'lang', 'sourceLanguage')); ?>
                    </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>

    </div>
    <div class="col-12">
        <label class="section-title"><?= Yii::t('app', "Системные настройки"); ?></label>
        <div class="row row-xs">
            <div class="col-12 col-lg-3 col-xl-2">
                <?= $form->field($model, 'maintenance')->widget(\kartik\select2\Select2::class, [
                    'data' => Yii::$app->params['switch'],
                    'theme' => \kartik\select2\Select2::THEME_DEFAULT,
                    'hideSearch' => true,
                    'pluginOptions' => [
                        'allowClear' => false
                    ],
                ]); ?>
                <?= $form->field($model, 'system_cache')->widget(\kartik\select2\Select2::class, [
                    'data' => Yii::$app->params['switch'],
                    'theme' => \kartik\select2\Select2::THEME_DEFAULT,
                    'hideSearch' => true,
                    'pluginOptions' => [
                        'allowClear' => false
                    ],
                ]); ?>
            </div>
            <div class="col-12 col-lg-9 col-xl-10">
                <div class="card bd-0">
                    <div class="card-header bg-primary">
                        <ul class="nav nav-tabs nav-tabs-for-dark card-header-tabs">
                            <li class="nav-item">
                                <a class="nav-link bd-0 active pd-y-8" href="#head_scripts" data-toggle="tab">
                                    <?= $model->getAttributeLabel('head_scripts'); ?>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link bd-0" href="#body_scripts" data-toggle="tab">
                                    <?= $model->getAttributeLabel('body_scripts'); ?>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link bd-0" href="#end_scripts" data-toggle="tab">
                                    <?= $model->getAttributeLabel('end_scripts'); ?>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="card-body bd bd-t-0">
                        <div class="tab-content">
                            <div class="tab-pane active" id="head_scripts">
                                <?= $form
                                    ->field($model, 'head_scripts', ['template' => '{label}{input}{error}{hint}'])
                                    ->textarea(['rows' => 6, 'class' => 'form-control fs-12'])->label(false); ?>
                            </div>
                            <div class="tab-pane" id="body_scripts">
                                <?= $form
                                    ->field($model, 'body_scripts', ['template' => '{label}{input}{error}{hint}'])
                                    ->textarea(['rows' => 6, 'class' => 'form-control fs-12'])->label(false); ?>
                            </div>
                            <div class="tab-pane" id="end_scripts">
                                <?= $form
                                    ->field($model, 'end_scripts', ['template' => '{label}{input}{error}{hint}'])
                                    ->textarea(['rows' => 6, 'class' => 'form-control fs-12'])->label(false); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-12 text-center">
    <button type="submit" class="btn btn-xl btn-outline-primary">
        <span class="fs-12 text-uppercase"><?= Yii::t('app', "Сохранить"); ?></span>
    </button>
</div>
<?php ActiveForm::end(); ?>
