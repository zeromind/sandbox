<?php /* @var $this yii\web\View */

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

use app\modules\cp\assets\TwoStepAsset;

TwoStepAsset::register($this);
$this->beginPage() ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Admin Dashboard">

    <!-- Meta -->
    <?= Html::csrfMetaTags() ?>

    <title><?= Html::encode($this->title) ?></title>

    <link rel="apple-touch-icon" sizes="180x180" href="/images/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/images/favicon/favicon-16x16.png">
    <link rel="manifest" href="/images/favicon/manifest.json">
    <link rel="mask-icon" href="/images/favicon/safari-pinned-tab.svg" color="#594df7">
    <link rel="shortcut icon" href="/images/favicon/favicon.ico">

    <meta name="msapplication-TileColor" content="#594df7">
    <meta name="msapplication-config" content="/images/favicon/browserconfig.xml">
    <meta name="theme-color" content="#858585">

    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="signin-wrapper">

    <?php
    /** @var \app\models\User $user */
    $user = Yii::$app->user->getIdentity();
    ?>
    <div class="signin-box">
        <p class="text-center">
            <i class="icon ion-locked tx-64"></i>
        </p>
        <h2 class="signin-title-primary text-center">Google Authenticator</h2>
        <h3 class="signin-title-secondary text-center tx-14"><?= $user->email; ?></h3>

        <?php $form = ActiveForm::begin(); ?>

        <div class="row row-xs mg-b-10">
            <div class="col-sm">
                <div class="form-group">
                    <?= $form->field($model, 'code', [
                        'template' => '{input}{error}{hint}',
                        'options' => [
                            'class' => 'input'
                        ]
                    ])->textInput([
                        'placeholder' => $model->getAttributeLabel('code'),
                        'class' => 'form-control text-center',
                        'data-mask' => "000 000"
                    ]); ?>
                </div>
            </div>
        </div><!-- row -->

        <?= Html::submitButton(
            Yii::t('app', "Войти в аккаунт"),
            ['class' => 'btn btn-primary btn-block btn-signin']
        ) ?>
        <?php ActiveForm::end(); ?>
    </div><!-- signin-box -->

</div><!-- signin-wrapper -->

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

