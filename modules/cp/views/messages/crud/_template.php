<?php /** @var $model \app\modules\cp\models\SourceMessage */
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;

$languages = \app\models\Languages::getBehaviorsList();

$form = ActiveForm::begin([
    'options' => ['enctype' => 'multipart/form-data'],
    'errorCssClass' => 'is-invalid',
    'fieldClass' => \yii\bootstrap4\ActiveField::class
]); ?>
<?= $form->errorSummary($model, ['class' => 'bd-l bd-3 bd-danger bg-gray-100 pd-x-20 pd-y-15 mg-b-20']); ?>
<div class="row mb-5">
    <div class="col-12 col-md-6 mx-auto">
        <div class="section-wrapper">
            <label class="section-title"><?= Yii::t('app', "Оригинал"); ?></label>
            <div class="row row-xs">
                <div class="col-12"><?= $form->field($model, 'category')->textInput(); ?></div>
                <div class="col-12"><?= $form->field($model, 'message')->textInput(); ?></div>
            </div>
            <label class="section-title"><?= Yii::t('app', "Перевод"); ?></label>
            <div class="card">
                <div class="card-header">
                    <ul class="nav nav-tabs card-header-tabs">
                        <?php $first = true; foreach ($languages as $title => $code) : ?>
                            <li class="nav-item">
                                <a class="nav-link<?php if ($first) : $first = false; ?> active<?php endif; ?>" href="#language-<?= $code; ?>" data-toggle="tab"><?= $title; ?></a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
                <div class="card-body">
                    <div class="tab-content">
                        <?php $first = true; foreach ($languages as $title => $code) : ?>
                            <div class="tab-pane<?php if ($first) : $first = false; ?> active<?php endif; ?>" id="language-<?= $code; ?>">
                                <?= Html::textInput('message[' . $code . ']', (isset($translations[$code])) ? $translations[$code] : '', ['class' => 'form-control']) ?>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>

            <div class="mg-t-20 text-right">
                <button type="submit" class="btn btn-xl btn-outline-primary">
                    <span class="fs-12 text-uppercase"><?= Yii::t('app', $model->isNewRecord ? "Добавить" : "Сохранить"); ?></span>
                </button>
            </div>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>
