<?php namespace app\modules\cp;

use Yii;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\cp\controllers';

    public function beforeAction($action)
    {
        /** @var \app\models\User $user */
        $user = Yii::$app->user->getIdentity();
        if ($user && !$user->verification && $action->controller->id != 'secure' && $action->id != 'two-step')
            return Yii::$app->response->redirect(['/cp/secure/two-step'])->send();

        return parent::beforeAction($action);
    }
}
