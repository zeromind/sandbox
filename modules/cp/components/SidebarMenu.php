<?php namespace app\modules\cp\components;

use Whoops\Exception\ErrorException;
use Yii;
use yii\helpers\FileHelper;

class SidebarMenu
{
    public $items = [];

    public $_system = [
        'ElFinder',
        'Lang',
        'Message',
        'Roles',
        'Translations',
        'Tune',
        'Watchdog'
    ];

    public function __construct()
    {
        $controllers_path = str_replace('app' , Yii::getAlias("@app"), Yii::$app->modules['cp']->controllerNamespace);
        if (PHP_OS == 'Linux') $controllers_path = str_replace('\\', '/', $controllers_path);

        if (is_dir($controllers_path))
        {
            $_files = FileHelper::findFiles($controllers_path);
            foreach ($_files as $_file)
            {
                $controller = str_replace([$controllers_path, '.php', '\\', 'Controller'], '', $_file);
                $controllerNamespace = $controller . 'Controller';
                $className = Yii::$app->modules['cp']->controllerNamespace . '\\' . basename($controllerNamespace, '.php');
                $controller_url = [];

                if (in_array($controller, $this->_system)) continue;

                preg_match_all('/[A-Z][^A-Z]*?/Us', $controller,$matches);
                foreach ($matches[0] as $match) if (!empty($match)) $controller_url[] = strtolower(trim($match));
                $controller_url = implode('-', $controller_url);

                $controllerSidebarSettings = new $className(0, Yii::$app->modules['cp']);

                if (method_exists($controllerSidebarSettings, 'sidebarSettings'))
                {
                    $settings = $controllerSidebarSettings->sidebarSettings();
                    if ($settings['visibility']) $this->items[] = [
                        'id'        => basename($controller),
                        'class'     => $className,
                        'url'       => $controller_url,
                        'settings'  => $settings
                    ];
                }
            }
        }
        else throw new ErrorException("Controllers path failure. " . $controllers_path);
    }

    public function items()
    {
        usort($this->items, function ($a, $b) {
            return $a['settings']['position'] - $b['settings']['position'];
        });

        if ($this->items && is_array($this->items) && count ($this->items) > 0)
        {
            $items = '';
            foreach ($this->items as $item)
            {
                $items .= Yii::$app->view->render('/sidebar/menu-item', [
                    'item' => $item,
                    'controller_id' => Yii::$app->controller->id,
                    'action_id' => Yii::$app->controller->action->id
                ]);
            }

            return $items;
        }

        return false;
    }
}