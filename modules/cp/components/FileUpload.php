<?php namespace app\modules\cp\components;

use Yii;
use yii\web\UploadedFile;
use app\models\UploadedFiles;

/**
 * Class FileUpload
 * @package app\modules\admin\components
 *
 * @property UploadedFile $uploadedFile
 * @property yii\db\ActiveRecord $model
 * @property array $extensions
 */
class FileUpload
{
    public $extensions = [
        'jpg', 'png', 'svg'
    ];

    public $uploadedFile;
    public $model;

    public $attribute;
    public $path = '/files/uploads/';

    public function __construct(UploadedFile $uploadedFile, yii\db\ActiveRecord $model, string $attribute, string $path = '/files/uploads/')
    {
        $this->uploadedFile = $uploadedFile;
        $this->model = $model;
        $this->attribute = $attribute;
        $this->path = $path;
    }

    public function saveAs()
    {
        $path = $this->path . time() . '_' . strtoupper(substr(md5($this->uploadedFile->baseName), 0, 10)) . '.' . $this->uploadedFile->extension;
        $_path_root = Yii::getAlias('@webroot') . $path;

        if ($this->validate($this->uploadedFile) && $this->validatePath(Yii::getAlias('@webroot') . $this->path) && $this->uploadedFile->saveAs($_path_root))
        {
            $saved = $this->model->getOldAttribute($this->attribute);
            if (!empty ($saved) && file_exists(Yii::getAlias('@webroot') . $saved))
                unlink(Yii::getAlias('@webroot') . $saved);

            $uploadedFiles = new UploadedFiles();
            $uploadedFiles->file = $path;
            if ($uploadedFiles->save())
            {
                $attribute = $this->attribute;
                $this->model->$attribute = $uploadedFiles->id;
            }
            else
            {
                $this->model->addError($this->attribute, Yii::t('app', "Не удалось сохранить файл"));
                if (file_exists($_path_root)) unlink($_path_root);
                return $this->model;
            }
        }
        else
        {
            $this->model->addError($this->attribute, Yii::t('app', "Не удалось загрузить файл"));
            return $this->model;
        }

        return $this->model;
    }

    private function validate(UploadedFile $uploadedFile)
    {
        if (!in_array($uploadedFile->extension, $this->extensions))
        {
            $this->model->addError($this->attribute, Yii::t('app', "Расширение файла не верное. ") . implode(', ', $this->extensions));
            return false;
        }

        return true;
    }

    private function validatePath($path)
    {
        return (
            file_exists($path . '/')
            || @mkdir($path . '/', 0777, true)
        );
    }
}