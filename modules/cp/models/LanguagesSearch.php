<?php namespace app\modules\cp\models;

use yii\data\ActiveDataProvider;
use app\models\Languages;

class LanguagesSearch extends Languages
{
    public function rules()
    {
        return [
            [['id', 'default'], 'integer'],
            [['locale', 'name'], 'safe'],
        ];
    }

    public function search($params)
    {
        $query = Languages::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => ['pageSize' => 30],
            'sort' => [
                'defaultOrder' => ['default' => SORT_DESC]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) return $dataProvider;

        $query
            ->andFilterWhere(['like', 'locale', $this->locale])
            ->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}