<?php namespace app\modules\cp\models;

use yii\data\ActiveDataProvider;

/**
 * Class UsersSearch
 * @package app\modules\cp\models
 *
 * @property string $full_name
 * @property string $role
 */
class UsersSearch extends User
{
    public $role;
    public $full_name;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status'], 'integer'],
            [['role'], 'string'],
            [['email'], 'safe'],
            [['phone'], 'string'],
            [['full_name'], 'safe']
        ];
    }

    public function search($params)
    {
        $query = User::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => ['pageSize' => 30],
			'sort' => [
			    'defaultOrder' => ['id' => SORT_DESC]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query
            ->andFilterWhere(['id' => $this->id, 'status' => $this->status])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->orFilterWhere(['like', 'first_name', $this->full_name])
            ->orFilterWhere(['like', 'second_name', $this->full_name])
            ->orFilterWhere(['like', 'last_name', $this->full_name])
            ->orFilterWhere(['like', 'nickname', $this->full_name])
            ->orFilterWhere(['like', 'email', $this->full_name]);

        if($this->role) $query
            ->join('LEFT JOIN','auth_assignment','auth_assignment.user_id = id')
            ->andFilterWhere(['auth_assignment.item_name' => $this->role]);

        return $dataProvider;
    }
}
