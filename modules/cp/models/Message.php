<?php namespace app\modules\cp\models;

use Yii;

/**
 * Class Message
 * @package app\modules\cp\models
 *
 * @property integer $id
 * @property string $language
 * @property string $translation
 */
class Message extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'message';
    }

    public function rules()
    {
        return [
            [['id', 'language'], 'required'],
            [['id'], 'integer'],
            [['translation'], 'string'],
            [['language'], 'string', 'max' => 16]
        ];
    }

    public function attributeLabels()
    {
        return [
            'language' => Yii::t('app', "Язык"),
            'translation' => Yii::t('app', "Перевод"),
        ];
    }

    public function getId0()
    {
        return $this->hasOne(SourceMessage::class, ['id' => 'id']);
    }
}