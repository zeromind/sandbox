<?php namespace app\modules\cp\models;

use Yii;
use yii\helpers\ArrayHelper;

use app\models\AuthItem;

/**
 * Class User
 * @package app\modules\cp\models
 *
 * @property bool $sendmail
 * @property bool $authenticator
 */
class User extends \app\models\User
{
    public $sendmail;
    public $authenticator;

    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            ['sendmail', 'string'],
            ['authenticator', 'string'],
            [['image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],

            [['first_name', 'last_name', 'nickname'], 'required']
        ]);
    }

    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'sendmail' => Yii::t('app', "Уведомить о регистрации")
        ]);
    }

    public function getRoles()
    {
        return $this->hasMany(AuthItem::class, ['name' => 'item_name'])->viaTable('auth_assignment', ['user_id' => 'id']);
    }

    public function getRolesList()
    {
        $roles = Yii::$app->authManager->getRoles();
        $rolesList = [];
        foreach ($roles as $index => $role) $rolesList[$index] = Yii::t('app', $role->description);

        return $rolesList;
    }

    public function getPermissionsList()
    {
        $permissions = [];
        $roles = Yii::$app->authManager->getRoles();

        if ($roles && count($roles)) foreach ($roles as $name => $role) $permissions[] = [
            'id' => $role->name,
            'text' => Yii::t('app', $role->description)
        ];

        return $permissions;
    }

    public function getPermissions()
    {
        $roles = Yii::$app->authManager->getRolesByUser($this->id);

        if ($roles && count($roles))
        {
            $permissions = [];
            foreach ($roles as $role) $permissions[$role->name] = '<span class="badge text-uppercase">' . Yii::t('app', $role->description) . '</span>';

            return implode(' ', $permissions);
        }

        return '<span class="badge text-uppercase ct-label">' . Yii::t('app', "Без роли") . '</span>';
    }
}