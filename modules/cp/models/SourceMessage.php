<?php namespace app\modules\cp\models;

use Yii;

/**
 * Class SourceMessage
 * @package app\modules\cp\models
 *
 * @property integer $id
 * @property string $message
 * @property string $category
 */
class SourceMessage extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'source_message';
    }

    public function rules()
    {
        return [
            [['category', 'message'], 'required'],
            [['message'], 'string'],
            [['category'], 'string', 'min' => 3, 'max' => 32]
        ];
    }

    public function attributeLabels()
    {
        return [
            'category' => Yii::t('app', 'Категория'),
            'message' => Yii::t('app', 'Сообщение')
        ];
    }

    public function getMessages()
    {
        return $this->hasMany(Message::class, ['id' => 'id']);
    }
}