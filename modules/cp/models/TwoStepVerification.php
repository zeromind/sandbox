<?php namespace app\modules\cp\models;

use Yii;
use yii\base\Model;

use app\components\GoogleAuthenticator;

class TwoStepVerification extends Model
{
    public $code;

    public function rules()
    {
        return [
            [['code'], 'required'],
            [['code'], 'string']
        ];
    }

    public function attributeLabels()
    {
        return [
            'code' => Yii::t('app', 'Код авторизации')
        ];
    }

    public function verify()
    {
        /** @var \app\models\User $user */
        $user = Yii::$app->user->getIdentity();
        $this->code = preg_replace('/\s+/', '', $this->code);

        $GoogleAuthenticator = new GoogleAuthenticator();
        if ($GoogleAuthenticator->verifyCode($user->google_authenticator_secret, $this->code)) return true;
        else
        {
            $this->addError('code', Yii::t('app', "Код авторизации не верный"));
            return false;
        }
    }
}