<?php namespace app\modules\cp\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class Settings
 * @package app\modules\cp\models
 */
class Settings extends \app\models\Settings
{
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['logotype'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg']
        ]);
    }

    public function beforeSave($insert)
    {
        $this->keywords = implode(",", $this->keywords);

        return parent::beforeSave($insert);
    }

    public function afterFind()
    {
        parent::afterFind();

        $this->keywords = explode(',', $this->keywords);
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        // Remove settings cache
        Yii::$app->cache->delete('app.settings');
    }
}