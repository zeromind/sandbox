<?php namespace app\modules\cp\models;

use yii\data\ActiveDataProvider;

/**
 * Class StaticPageSearch
 * @package app\modules\cp\models
 *
 * @property string $title
 * @property integer $views
 * @property integer $public
 * @property integer $url
 */
class StaticPageSearch extends StaticPage
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'views', 'public'], 'integer'],
            [['title', 'url'], 'string']
        ];
    }

    public function search($params)
    {
        $query = StaticPage::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => ['pageSize' => 30],
            'sort' => [
                'defaultOrder' => ['id' => SORT_DESC]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query
            ->andFilterWhere(['id' => $this->id, 'public' => $this->public])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'url', $this->url])
            ->orFilterWhere(['>=', 'views', $this->views]);

        return $dataProvider;
    }
}
