<?php namespace app\modules\cp\models;

use yii\data\ActiveDataProvider;

class SourceMessageSearch extends SourceMessage
{
    public function rules()
    {
        return [
            [['category', 'message'], 'safe'],
        ];
    }

    public function search($params)
    {
        $query = SourceMessage::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort'=> ['defaultOrder' => ['message' => SORT_ASC]]
        ]);

        $this->load($params);

        if (!$this->validate()) return $dataProvider;

        $query
            ->andFilterWhere(['id' => $this->id])
            ->andFilterWhere(['like', 'category', $this->category])
            ->andFilterWhere(['like', 'message', $this->message]);

        return $dataProvider;
    }
}