<?php namespace app\modules\cp\models;

use Yii;
use yii\helpers\ArrayHelper;

class StaticPage extends \app\models\StaticPage
{
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'title_en' => Yii::t('app', "Заголовок"),
            'title_uk' => Yii::t('app', "Заголовок"),

            'content_en' => Yii::t('app', "Контент"),
            'content_uk' => Yii::t('app', "Контент"),
        ]);
    }
}