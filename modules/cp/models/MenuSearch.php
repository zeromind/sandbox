<?php namespace app\modules\cp\models;

use yii\data\ActiveDataProvider;
use app\models\Menu;

class MenuSearch extends Menu
{
    public function rules()
    {
        return [
            [['id', 'parent_id', 'sort_order', 'depth', 'group'], 'integer'],
            [['name', 'url'], 'safe']
        ];
    }

    public function search($params)
    {
        $query = Menu::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => ['pageSize' => 30],
            'sort' => [
                'defaultOrder' => ['sort_order' => SORT_ASC]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) return $dataProvider;

        $query
            ->andFilterWhere(['id' => $this->id])
            ->andFilterWhere(['=', 'parent_id', $this->parent_id])
            ->andFilterWhere(['=', 'sort_order', $this->sort_order])
            ->andFilterWhere(['=', 'depth', $this->depth])
            ->andFilterWhere(['=', 'group', $this->group])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'url', $this->url]);

        return $dataProvider;
    }
}