<?php namespace app\modules\frontend\assets;

class AppAsset extends \yii\web\AssetBundle
{
    public $basePath = '@webroot/modules/frontend/assets';
    public $baseUrl = '@web/porto-730/html';

    public $css = [
        'vendor/fontawesome-free/css/all.min.css',
        'vendor/animate/animate.min.css',
        'vendor/simple-line-icons/css/simple-line-icons.min.css',
        'vendor/owl.carousel/assets/owl.carousel.min.css',
        'vendor/owl.carousel/assets/owl.theme.default.min.css',
        'vendor/magnific-popup/magnific-popup.min.css',
        'master/css/theme.css',
        'master/css/theme-elements.css',
        'master/css/theme-blog.css',
        'master/css/theme-shop.css',

        'master/css/skins/default.css',
        'master/css/util.css',
        'master/css/custom.css',
        '/css/frontend-custom.css',
    ];

    public $js = [
        'vendor/jquery.appear/jquery.appear.min.js',
        'vendor/jquery.easing/jquery.easing.min.js',
        'vendor/jquery.cookie/jquery.cookie.min.js',
        'vendor/common/common.min.js',
        'vendor/jquery.validation/jquery.validate.min.js',
        'vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js',
        'vendor/jquery.gmap/jquery.gmap.min.js',
        'vendor/jquery.lazyload/jquery.lazyload.min.js',
        'vendor/isotope/jquery.isotope.min.js',
        'vendor/owl.carousel/owl.carousel.min.js',
        'vendor/magnific-popup/jquery.magnific-popup.min.js',
        'vendor/vide/jquery.vide.min.js',
        'vendor/vivus/vivus.min.js',
        '/slimadmin-11/template/app/lib/jquery.mask/jquery.mask.min.js',

        'js/theme.js',
        'js/custom.js',
        'js/theme.init.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap4\BootstrapAsset',
        'yii\bootstrap4\BootstrapPluginAsset'
    ];

    public function init()
    {
        parent::init();

        \Yii::$app->assetManager->bundles['yii\\web\\JqueryAsset'] = [
            'basePath' => '@webroot',
            'baseUrl' => '@web/porto-730/html',
            'css' => ['/css/basscss.min.css'],
            'js' => ['vendor/jquery/jquery.min.js']
        ];
    }
}