<?php namespace app\modules\frontend\assets;

use yii\web\AssetBundle;

class BlockedAsset extends AssetBundle
{
    public $basePath = '@webroot/modules/frontend/assets';
    public $baseUrl = '@web/slimadmin-11/template/app';

    public $css = [
        'lib/font-awesome/css/font-awesome.css',
        'lib/Ionicons/css/ionicons.css',
        'css/slim.css'
    ];

    public $js = [];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap4\BootstrapAsset',
        'yii\bootstrap4\BootstrapPluginAsset',
    ];
}