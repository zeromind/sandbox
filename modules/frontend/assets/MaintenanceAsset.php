<?php namespace app\modules\frontend\assets;

use yii\web\AssetBundle;

class MaintenanceAsset extends AssetBundle
{
    public $basePath = '@webroot/modules/frontend/assets';
    public $baseUrl = '@web/maintenance';

    public $css = [
        'vendor/bootstrap/css/bootstrap.min.css',
        'fonts/font-awesome-4.7.0/css/font-awesome.min.css',
        'vendor/animate/animate.css',
        'vendor/select2/select2.min.css',
        'vendor/countdowntime/flipclock.css',
        'css/util.css',
        'css/main.css'
    ];

    public $js = [
        'vendor/select2/select2.min.js',
        'vendor/countdowntime/flipclock.min.js',
        'vendor/countdowntime/moment.min.js',
        'vendor/countdowntime/moment-timezone.min.js',
        'vendor/countdowntime/moment-timezone-with-data.min.js',
        'vendor/countdowntime/countdowntime.js',
        'vendor/tilt/tilt.jquery.min.js',
        'js/main.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap4\BootstrapAsset',
        'yii\bootstrap4\BootstrapPluginAsset',
    ];
}