<?php namespace app\modules\frontend\controllers;

use Yii;
use yii\web\NotFoundHttpException;

use app\components\Controller;
use app\models\StaticPage;

class DefaultController extends Controller
{
    public $layout = './main';

	public function actions()
	{
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'timezone' => [
                'class' => 'yii2mod\timezone\TimezoneAction',
            ],
        ];
	}

    /**
     * @param bool $page
     * @return string
     * @throws NotFoundHttpException
     */
	public function actionIndex($page = false)
	{
	    if ($page) {
            $page = $this->getStaticPage($page);
            if ($page) return $this->render('/static-page', ['model' => $page]);
            else throw new NotFoundHttpException(Yii::t('app', "К сожалению страница, которую вы ищите, не найдена."), 404);
        }

        return $this->render('/front-page');
	}

    /**
     * @param $url
     * @return mixed
     */
	private function getStaticPage($url)
    {
        $page_query = StaticPage::find()->where(['=', 'url', $url]);
        if (!Yii::$app->user->identity->hasPermission(['admin', 'moderator'])) {
            $page_query->andFilterWhere(['=', 'public', 1]);
        }
        $page = $page_query->one();

        return $page;
    }
}