<?php /** @var \yii\web\View $this */

use app\models\Menu;
use yii\widgets\Menu as Navigation; ?>
<header id="header" class="header-effect-shrink" data-plugin-options="{'stickyEnabled': true, 'stickyEffect': 'shrink', 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyChangeLogo': true, 'stickyStartAt': 30, 'stickyHeaderContainerHeight': 70}">
    <div class="header-body bg-color-dark-scale-1 border-0 box-shadow-none">
        <div class="header-container container">
            <div class="header-row">
                <div class="header-column">
                    <div class="header-row">
                        <div class="header-logo mt-0 mb-0">
                            <a href="<?= \yii\helpers\Url::to(['/']); ?>">
                                <img width="100" height="100" data-sticky-width="80" data-sticky-height="80" src="<?= Yii::$app->controller->settings->logotypeUrl; ?>" alt="Logo">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="header-column justify-content-end">
                    <div class="header-row">
                        <div class="header-nav header-nav-links header-nav-force-light-on-dark-text header-nav-force-light-text-active-skin-color order-2 order-lg-0">
                            <div class="header-nav-main header-nav-main-square header-nav-main-dropdown-no-borders header-nav-main-dropdown-arrow header-nav-main-effect-3 header-nav-main-sub-effect-1">
                                <nav class="collapse">
                                    <?= Navigation::widget([
                                        'items' => Menu::getMenuItems()[Menu::GROUP_HEADER],
                                        'options' => [
                                            'class' => 'nav nav-pills'
                                        ],
                                        'submenuTemplate' => '<ul class="dropdown-menu">{items}</ul>'
                                    ]); ?>
                                </nav>
                            </div>
                            <button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main nav">
                                <i class="fas fa-bars"></i>
                            </button>
                        </div>
                        <div class="header-nav-features header-nav-features-no-border header-nav-features-lg-show-border order-1 order-lg-2">
                            <?= $this->render('@app/modules/user/views/profile'); ?>
                        </div>
                        <?= \app\components\widgets\Languages::widget(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

