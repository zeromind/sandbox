<?php /* @var $this yii\web\View */

use yii\helpers\Html;
use app\modules\frontend\assets\AppAsset;

AppAsset::register($this);
$this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <!-- Basic -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Meta -->
    <?= Html::csrfMetaTags() ?>

    <title><?= Html::encode($this->title) ?></title>

    <link rel="apple-touch-icon" sizes="180x180" href="/images/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/images/favicon/favicon-16x16.png">
    <link rel="manifest" href="/images/favicon/manifest.json">
    <link rel="mask-icon" href="/images/favicon/safari-pinned-tab.svg" color="#594df7">
    <link rel="shortcut icon" href="/images/favicon/favicon.ico">

    <meta name="msapplication-TileColor" content="#594df7">
    <meta name="msapplication-config" content="/images/favicon/browserconfig.xml">
    <meta name="theme-color" content="#858585">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">

    <!-- Web Fonts  -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">

    <?php $this->head() ?>

</head>
<?php
$preloaded = Yii::$app->getSession()->get('preloader-performed');
if ($preloaded && $preloaded == 'yes') : ?><body>
<?php else : Yii::$app->getSession()->set('preloader-performed', 'yes'); ?><body class="loading-overlay-showing" data-loading-overlay data-plugin-options="{'hideDelay': 500}">
<div class="loading-overlay">
    <div class="bounce-loader">
        <div class="bounce1"></div>
        <div class="bounce2"></div>
        <div class="bounce3"></div>
    </div>
</div>
<?php endif; ?>
<?php $this->beginBody() ?>
    <div class="body">
        <?= $this->render('_header'); ?>
        
        <div role="main" class="main" style="min-height: calc(100vh - 415px - 150px);">
            <div class="alerts--wrapper"><?= \app\components\widgets\Alert::widget(); ?></div>
            <?= $content ?>
        </div>

        <?= $this->render('_footer'); ?>
    </div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
