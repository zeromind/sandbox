<footer id="footer">
    <div class="container icon-aria-expanded-change cur-pointer" data-toggle="collapse" href="#footer-collapse" role="button" aria-expanded="false" aria-controls="footer-collapse">
        <div class="row justify-content-center py-4 text-center">
            <div class="col">
                <span class="text-uppercase font-weight-bold text-3 text-color-light">
                    <i class="ml-1 fas fa-chevron-down"></i>
                    <i class="ml-1 fas fa-chevron-up"></i>
                </span>
            </div>
        </div>
    </div>
    <div class="collapse bg-color-dark-scale-2 footer-bottom-light-border" id="footer-collapse">
        <div class="container">
            <div class="py-5 text-center text-color-light">TEXT</div>
        </div>
    </div>
    <div class="footer-bg-color-2 footer-bottom-light-border">
        <div class="container">
            <div class="row justify-content-center text-center py-5">
                <div class="col-md-4 text-center mb-4 mb-md-0">
                    <i class="fas fa-map-marker-alt text-9 text-color-light mb-3 mt-2"></i>
                    <p class="mb-0">
                        <strong class="text-color-light text-uppercase">Porto Address</strong>
                        <span class="d-block text-2 p-relative bottom-3">123 Street Name, City, ST</span>
                    </p>
                </div>
                <div class="col-md-4 text-center mb-4 mb-md-0">
                    <i class="far fa-clock text-9 text-color-light mb-3 mt-2"></i>
                    <p class="mb-0">
                        <strong class="text-color-light text-uppercase">Business Hours</strong>
                        <span class="d-block text-2 p-relative bottom-3">Mon - Sun / 9:00AM - 8:00PM</span>
                    </p>
                </div>
                <div class="col-md-4 text-center">
                    <i class="fas fa-phone-volume text-9 text-color-light mb-3 mt-2"></i>
                    <p class="mb-0">
                        <strong class="text-color-light text-uppercase">Call Us Now</strong>
                        <span class="d-block text-2 p-relative bottom-3">(800) 123-4567</span>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <div class="container py-2">
            <div class="row py-4">
                <div class="col text-center">
                    <ul class="footer-social-icons social-icons social-icons-clean social-icons-icon-light mb-3">
                        <li class="social-icons-facebook"><a href="http://www.facebook.com/" target="_blank" title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
                        <li class="social-icons-twitter"><a href="http://www.twitter.com/" target="_blank" title="Twitter"><i class="fab fa-twitter"></i></a></li>
                        <li class="social-icons-linkedin"><a href="http://www.linkedin.com/" target="_blank" title="Linkedin"><i class="fab fa-linkedin-in"></i></a></li>
                    </ul>
                    <p>© Copyright 2019. All Rights Reserved.</p>
                </div>
            </div>
        </div>
    </div>
</footer>