<?php /**
 * @var yii\web\View $this
 * @var \app\models\StaticPage $model
 */ ?>
<section class="page-header page-header-modern bg-color-light-scale-1 page-header-md">
    <div class="container">
        <div class="row">
            <div class="col-md-8 order-2 order-md-1 align-self-center p-static">
                <h1 class="text-dark"><?= $model->title; ?></h1>
            </div>
        </div>
    </div>
</section>
<div class="container">
    <?= \yii\helpers\Html::decode($model->content); ?>
</div>