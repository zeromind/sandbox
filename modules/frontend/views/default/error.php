<?php
/**
 * @var \yii\web\View $this
 * @var \Exception $exception
 */

use yii\widgets\Breadcrumbs;

$title = Yii::t('app', "Ошибка");
Yii::$app->controller->setTitle($title);
$this->params['breadcrumbs'][] = $title; ?>
<section class="page-header page-header-modern bg-color-secondary page-header-sm">
    <div class="container">
        <div class="row">
            <div class="col-md-8 order-2 order-md-1 align-self-center p-static text-uppercase">
                <h1>Ошибка <strong><?= $exception->getCode() ? $exception->getCode() : 404; ?></strong></h1>
            </div>
            <div class="col-md-4 order-1 order-md-2 align-self-center">
                <?= Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    'options' => [
                        'class' => 'breadcrumb d-block text-md-right breadcrumb-light'
                    ],
                    'itemTemplate' => '<li>{link}</li>',
                    'activeItemTemplate' => '<li class="active">{link}</li>'
                ]) ?>
            </div>
        </div>
    </div>
</section>

<div class="container">
    <div class="card border-radius-0 bg-color-light border-0 box-shadow-1">
        <div class="card-body">
            <section class="http-error py-0">
                <div class="row justify-content-center py-3">
                    <div class="col-6 text-center">
                        <div class="http-error-main py-3">
                            <img class="icon-animated" width="180" src="/porto-730/html/vendor/linea-icons/linea-basic/icons/basic-exclamation.svg" alt="" data-icon data-plugin-options="{'color': '#E36159', 'animated': true, 'delay': 200, 'strokeBased': true}" />
                            <p class="text-6 font-weight-bold mt2 mb0 text-color-dark"><?= $exception->getCode() ? $exception->getCode() : 404; ?></p>
                            <p class="text-2 text-uppercase"><?= $exception->getMessage(); ?></p>
                        </div>
                        <a class="btn btn-rounded btn-primary btn-xl btn-with-arrow mb-4" href="<?= \yii\helpers\Url::to(['/']); ?>">
                            На главную
                            <span><i class="fas fa-angle-right"></i></span>
                        </a>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>


