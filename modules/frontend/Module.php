<?php namespace app\modules\frontend;

class Module extends \app\components\BaseModule
{
    public $controllerNamespace = 'app\modules\frontend\controllers';
}
