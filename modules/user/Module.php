<?php namespace app\modules\user;

class Module extends \app\components\BaseModule
{
    public $controllerNamespace = 'app\modules\user\controllers';
}
