<?php namespace app\modules\user\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\helpers\Url;

use app\components\Controller;
use app\components\GoogleAuthenticator;
use app\modules\user\models\SignInForm;
use app\modules\user\models\SignUpForm;
use app\modules\user\models\TwoStepForm;

use app\models\User;

class DefaultController extends Controller
{
    public $layout = '@frontend/layouts/main';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['sign-out'],
                'rules' => [
                    [
                        'actions' => ['confirmation-email'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['sign-in', 'sign-up'],
                        'allow' => true,
                        'roles' => ['?']
                    ],
                    [
                        'actions' => ['sign-out', 'confirmation-two-step'],
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ]
            ]
        ];
    }

    public static function getAllowedDirections()
    {
        return ['sign-in', 'sign-out'];
    }

    public function actionConfirmationTwoStep()
    {
        if (Yii::$app->user->isGuest) return $this->redirect(Url::toRoute(['/user/default/sign-in']));

        $model = new TwoStepForm();
        if ($model->load(Yii::$app->request->post()) && $model->verify())
        {
            Yii::$app->getSession()->setFlash('success', Yii::t('app', "Вы успешно подтвердили <b>Google Authenticator</b>"));
            return $this->goHome();
        }
        else return $this->render('two-step', [
            'model' => $model
        ]);
    }

    public function actionConfirmationEmail($email, $token)
    {
        $user = User::findByEmail($email);
        if ($user && !$user->email_verified && !empty($user->email_confirm_token) && $user->email_confirm_token == $token)
        {
            $user->email_verified = 1;
            $user->email_confirm_token = null;

            // Set Google Authenticator secret
            $GoogleAuthenticator = new GoogleAuthenticator();
            $user->google_authenticator_secret = $GoogleAuthenticator->createSecret();

            if ($user->validate() && $user->save())
            {
                Yii::$app->mailer->compose('templates/user/verified', [
                    'QR_CODE' => $GoogleAuthenticator->getQRCodeGoogleUrl(Yii::$app->name, $user->google_authenticator_secret, Yii::t('app', "{app} | {email}", [
                        'app' => Yii::$app->name,
                        'email' => $user->email
                    ]), [
                        'width' => 500,
                        'height' => 500,
                        'level' => 'H'
                    ]),
                    'SECURITY_CODE' => $user->google_authenticator_secret,
                    'URL' => Yii::getAlias('@baseUrl') . Url::to(['/user/default/confirmation-two-step'])
                ])
                    ->setFrom([Yii::$app->params['email']['sign-up'] => Yii::$app->name])
                    ->setTo($user->email)
                    ->setSubject(Yii::t('app', "Регистрация на сайте " . Yii::$app->name))
                    ->send();

                // Authorize user
                $user->setAuthKey();
                Yii::$app->user->login($user, 3600*24*7);

                Yii::$app->getSession()->setFlash('success', Yii::t('app', "Ваш адрес электронной почты подтвержден"));
                return $this->goHome();
            }
        }

        Yii::$app->getSession()->setFlash('success', Yii::t('app', "Ваш адрес электронной почты подтвержден"));
        return $this->goHome();
    }

    public function actionSignUp()
    {
        $this->layout = 'sign';

        if (!Yii::$app->user->isGuest) return $this->goHome();

        $model = new SignUpForm();
        if ($model->load(Yii::$app->request->post()) && $model->register())
        {
            Yii::$app->getSession()->setFlash('success', Yii::t('app', "На вашу почту отправлено письмо с подтверждением"));
            return $this->goBack();
        }
        else return $this->render('sign-up', [
            'model' => $model
        ]);
    }

    public function actionSignIn()
    {
        $this->layout = 'sign';

        if (!Yii::$app->user->isGuest) return $this->goHome();

        $model = new SignInForm();
        if ($model->load(Yii::$app->request->post()) && $model->authorization()) return $this->goBack();
        else return $this->render('sign-in', [
            'model' => $model
        ]);
    }

    public function actionSignOut()
    {
        Yii::$app->user->logout();
        return $this->goHome();
    }
}