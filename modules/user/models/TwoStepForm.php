<?php namespace app\modules\user\models;

use Yii;
use yii\base\Model;

use app\components\GoogleAuthenticator;

class TwoStepForm extends Model
{
    public $code;

    public function rules()
    {
        return [
            [['code'], 'required'],
            [['code'], 'string']
        ];
    }

    public function attributeLabels()
    {
        return [
            'code' => Yii::t('app', 'Код авторизации')
        ];
    }

    public function verify()
    {
        /** @var \app\models\User $user */
        $user = Yii::$app->user->getIdentity();
        $GoogleAuthenticator = new GoogleAuthenticator();

        preg_match_all('!\d+!', $this->code, $matches);
        $this->code = implode(false, $matches[0]);

        if ($GoogleAuthenticator->verifyCode($user->google_authenticator_secret, $this->code)) return true;
        else $this->addError('code', Yii::t('app', "Код авторизации не верный"));

        return false;
    }
}