<?php namespace app\modules\user\models;

use Yii;
use yii\base\Model;

use app\components\GoogleAuthenticator;

/**
 * Class SignInForm
 * @package app\modules\user\models
 *
 * @property \app\models\User $user
 */
class SignInForm extends Model
{
    public $identity = false;
    public $email;
    public $code;

    public function rules()
    {
        return [
            [['email', 'code'], 'required'],
            [['email'], 'email'],
            [['code'], 'string']
        ];
    }

    public function attributeLabels()
    {
        return [
            'email'     => Yii::t('app', 'Эл. почта'),
            'code'      => Yii::t('app', 'Код авторизации')
        ];
    }

    public function authorization()
    {
        if (isset ($this->code) && !empty($this->code))
        {
            preg_match_all('!\d+!', $this->code, $matches);
            $this->code = implode(false, $matches[0]);

            if ($this->user)
            {
                $GoogleAuthenticator = new GoogleAuthenticator();
                if ($this->user->google_authenticator_secret)
                {
                    if ($GoogleAuthenticator->verifyCode($this->user->google_authenticator_secret, $this->code))
                    {
                        $this->user->setAuthKey();
                        Yii::$app->getSession()->set('two-step-expired', time() + 84600);
                        return Yii::$app->user->login($this->user);
                    }
                    else $this->addError('code', Yii::t('app', "Код не совпадает"));
                }
                else $this->addError('code', Yii::t('app', "Привяжите Google Authenticator"));
            }
            else $this->addError('email', Yii::t('app', "Пользователь не найден"));
        }
        else $this->addError('code', Yii::t('app', "Укажите код подтверждения"));

        return false;
    }

    public function getUser()
    {
        if ($this->identity === false) $this->identity = User::findByEmail($this->email);
        return $this->identity;
    }
}