<?php namespace app\modules\user\models;

use Yii;
use yii\base\Model;
use yii\helpers\Url;

class SignUpForm extends Model
{
    public $email;

    public function rules()
    {
        return [
            [['email'], 'required'],
            ['email', 'email'],
            [['email'], 'string']
        ];
    }

    public function attributeLabels()
    {
        return [
            'email' => Yii::t('app', 'Эл. почта')
        ];
    }

    public function register()
    {
        $user = new User();
        $user->email = $this->email;
        $user->email_confirm_token = Yii::$app->security->generateRandomString(16);

        if ($user->validate() && $user->save())
        {
            Yii::$app->mailer->compose('templates/user/sign-up', [
                'CONFIRM_URL' => Yii::getAlias('@baseUrl') . Url::to(['/user/default/confirmation-email', 'email' => $user->email, 'token' => $user->email_confirm_token])
            ])
                ->setFrom([Yii::$app->params['email']['sign-up'] => Yii::$app->name])
                ->setTo($user->email)
                ->setSubject(Yii::t('app', "Регистрация на сайте " . Yii::$app->name))
                ->send();

            return true;
        }
        elseif ($user->hasErrors())
        {
            foreach ($user->errors as $attribute => $error)
            {
                $this->addError($attribute, $error[0]);
            }
        }

        return false;
    }
}