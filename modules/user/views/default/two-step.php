<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap4\ActiveForm */

use app\modules\frontend\assets\AppAsset;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;

$title = Yii::t('app', "Google Authenticator");
Yii::$app->controller->setTitle($title);

$bundle = AppAsset::register($this); ?>
<div class="featured-boxes featured-boxes-style-3 featured-boxes-flat d-flex justify-content-center">
    <div class="row">
        <div class="col" style="max-width: 400px;">
            <div class="featured-box featured-box-quaternary border-0 featured-box-effect-3">
                <div class="box-content box-content-border-0 box-shadow-1 box-shadow-1-quaternary">
                    <i class="icon-featured icon-key icons"></i>
                    <h4 class="font-weight-normal text-5 mt-3"><strong class="font-weight-extra-bold"><?= $title; ?></strong></h4>
                    <p class="mb-2 mt-2 text-2"><?= Yii::t('app', "Верифицируйте аккаунт, что бы иметь возможность им пользоватся. Google Authenticator позволяет избежать не нужных паролей"); ?></p>

                    <div class="z-index-1 p-relative">
                        <?php $form = ActiveForm::begin(); ?>
                        <div class="row mb2">
                            <div class="col">
                                <?= $form->field($model, 'code', [
                                    'template' => '{input}{error}{hint}',
                                    'options' => [
                                        'class' => 'input'
                                    ]
                                ])->textInput([
                                    'placeholder' => $model->getAttributeLabel('code'),
                                    'class' => 'form-control text-center',
                                    'data-mask' => "000 000"
                                ]); ?>
                            </div>
                        </div><!-- row -->

                        <?= Html::submitButton(
                            Yii::t('app', "Подтвердить"),
                            ['class' => 'btn btn-primary btn-block btn-signin']
                        ) ?>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>