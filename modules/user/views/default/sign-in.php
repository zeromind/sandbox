<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap4\ActiveForm */

use app\modules\frontend\assets\AppAsset;

use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

$title = Yii::t('app', "Авторизация");
Yii::$app->controller->setTitle($title);

$bundle = AppAsset::register($this); ?>
<div class="featured-boxes featured-boxes-style-3 featured-boxes-flat d-flex justify-content-center">
    <div class="row">
        <div class="col mt-5 pt-5" style="min-width: 400px;">
            <div class="featured-box featured-box-quaternary border-0 featured-box-effect-3">
                <div class="box-content box-content-border-0 box-shadow-1 box-shadow-1-quaternary">
                    <i class="icon-featured icon-lock icons"></i>
                    <h4 class="font-weight-normal text-5 mt-3 mb-3"><strong class="font-weight-extra-bold"><?= $title; ?></strong></h4>
                    <hr class="border-top-gray" />
                    <div class="z-index-1 p-relative">
                        <?php $form = ActiveForm::begin(); ?>
                        <div class="row row-xs mg-b-10">
                            <div class="col-sm">
                                <div class="form-group">
                                    <?= $form->field($model, 'email', [
                                        'template' => '{input}{error}{hint}',
                                        'options' => [
                                            'class' => 'input'
                                        ]
                                    ])->textInput([
                                        'placeholder' => $model->getAttributeLabel('email'),
                                        'class' => 'form-control text-center'
                                    ]); ?>
                                </div>
                                <div class="form-group">
                                    <?= $form->field($model, 'code', [
                                        'template' => '{input}{error}{hint}',
                                        'options' => [
                                            'class' => 'input'
                                        ]
                                    ])->textInput([
                                        'placeholder' => $model->getAttributeLabel('code'),
                                        'class' => 'form-control text-center',
                                        'data-mask' => "000 000"
                                    ]); ?>
                                </div>
                            </div>
                        </div><!-- row -->

                        <div class="row justify-content-md-center">
                            <div class="col col-lg-6">
                                <?= Html::submitButton(
                                    Yii::t('app', "Войти"),
                                    ['class' => 'btn btn-primary btn-block btn-signin']
                                ) ?>
                            </div>
                        </div>

                        <?php ActiveForm::end(); ?>
                        <div class="row mt-5">
                            <div class="col-6 text-left p-l-25">
                                <a href="javascript:void();" class="fs-10 text-uppercase text-color-secondary" onclick="window.history.back(); return false;">
                                    <?= Yii::t('app', "Назад"); ?>
                                </a>
                            </div>
                            <div class="col-6 text-right p-r-25">
                                <a href="<?= Url::toRoute(['/user/default/sign-up']); ?>" class="fs-10 text-uppercase text-color-hover-quaternary">
                                    <?= Yii::t('app', "Создать аккаунт"); ?>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<div class="row justify-content-md-center">
    <div class="col col-lg-4 text-center">
        <a href="<?= Url::toRoute(['/']); ?>" class="fs-10 text-uppercase text-color-light">
            <?= Yii::t('app', "На главную"); ?>
        </a>
    </div>
</div>