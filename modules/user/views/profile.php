<?php
/**
 * @var \yii\web\View $this
 * @var \app\models\User $user
 */

use yii\helpers\Url;

$user = Yii::$app->user->getIdentity();
if (Yii::$app->user->isGuest) : ?>
    <a href="<?= Url::toRoute(['/user/default/sign-in']); ?>" class="btn btn-modern btn-primary btn-badge">
        <i class="icon-lock icons text-5"></i>
        <span class="badge badge-dark badge-sm badge-pill font-weight-light text-uppercase py-1">ALPHA</span>
    </a>
<?php else : ?>
    <div class="header-nav-feature header-nav-features-user header-nav-features-user-logged d-inline-flex">
        <a href="#" class="header-nav-features-toggle text-color-light">
            <img width="40" class="img-fluid rounded-circle p-1 bg-color-dark-scale-5" src="<?= $user->photoURL; ?>" /> &nbsp; <?= $user->nickname; ?>
        </a>
        <div class="header-nav-features-dropdown header-nav-features-dropdown-mobile-fixed profile-dropdown">
            <ul class="nav nav-list flex-column">
                <?php if ($user->hasPermission(['admin', 'moderator'])) : ?><li class="nav-item">
                    <a class="nav-link text-color-light bg-color-hover-dark" href="<?= Url::to(['/cp/dashboard/index']); ?>">
                        <?= Yii::t('app', "Панель управления"); ?>
                    </a>
                </li>
                <?php endif; ?>
                <li class="nav-item">
                    <a class="nav-link text-color-dark bg-color-secondary" href="<?= Url::to(['/user/default/sign-out']); ?>">
                        <?= Yii::t('app', "Выход"); ?>
                    </a>
                </li>
            </ul>
        </div>
    </div>
<?php endif; ?>